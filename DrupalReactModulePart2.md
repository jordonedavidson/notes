# Building a Drupal Module with Embedded React

## Part 2: Adding in the Template

**Goals**

- generate a basic twig template for the module
- connect it to the route
- modify it to our needs

## Generate the Template

The `drush generate` command has a tonne of options, the one we want this time is `template`.

By default the generate template command will create both theme and preprocess hooks. These will land in the mta_staff_faculty_directory.module file. I don't know if they will be necessary at this point so I'm going to say yes to those prompts. Also for simplicity of naming, I'm calling the template 'directory' instead of using the full machine name of the plugin.

```console
./vendor/bin/drush generate template

 Welcome to template generator!
––––––––––––––––––––––––––––––––

 Module machine name:
 ➤ mta_staff_faculty_directory

 Template name [example]:
 ➤ directory

 Create theme hook? [Yes]:
 ➤

 Create preprocess hook? [Yes]:
 ➤

 The following directories and files have been created or updated:
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 • modules/custom/mta_staff_faculty_directory/mta_staff_faculty_directory.module
 • modules/custom/mta_staff_faculty_directory/templates/directory.html.twig
```

## Connecting the Theme to the Route

In the Controller file `/src/Controller/MtaStaffFacultyDirectoryController.php` The build method is responsible for outputting the content.

There can be all sorts of logic in this method, but all we need to do is tell Drupal to use the directory theme we just created. The updated method looks like this:

```php
public function build() {

  $build['content'] = [
    '#theme' => 'directory',
  ];

  return $build;
}
```

Very simple.

The theme and preprocess hooks in the .module file now take over and push the variable `foo` to the directory template. At this point our directory page simply says "bar" which is the value of foo set in the `template_preprocess_directory` function. So everything is working as expected.

**IMPORTANT** Remember to use `./vendor/bin/drush cr` or you won't see any changes.

## Making Sense of the Hooks

Earier I mentioned that I wasn't sure I'd need the hooks that were provided by the generate template process. Let's take a deeper dive into them to see what functionality they provide and if we will ultimatly need it.

**_Note:_** the word "hook" is repaced by the machine name of the module.

### hook_theme

Out of the box the theme hook looks like this:

```php
/**
 * Implements hook_theme().
 */
function mta_staff_faculty_directory_theme() {
  return [
    'directory' => [
      'variables' => ['foo' => NULL],
    ],
  ];
}
```

In this array `directory` is the name of the twig theme (`/templates/directory.html.twig`) and `variables` represents the default values for the variables the template expects to receive. In many cases these variables come from the varous entities that the module is going to display (pages, posts, etc.) In this case our only variable is `foo`

Here is the where it is used in the generated twig template:

```twig
<div class="wrapper-class">
  {{ foo }}
</div>
```

The `template_preprocess_HOOK` function takes the variables array and maniplulates it to set up the default values. Here "HOOK" is replaced by the template name.

Our default function `template_preprocess_directory` sets `foo` to `bar` which we see displayed on the page.

Since this module will be displaying the React app, it turns out that we will not need these hooks after all.

## Updating the .twig Template

Ultimately all we need to output is a div with an id parameter for our React app to bind to. And if there was nothing else that we needed to display that could be handled without a template by passing the div as the value of the `#markup` key of the array returned by the contoller's `build` method.

In this case our app uses a number of SVG icons, so we need to place those in the template for easy access. Her is the updated twig template:

```twig
{#
/**
 * @file
 * Default theme implementation to display the React app.
 * SVG icons are loaded here to avoid polluting the website.
 * The app will hook into the '#mta_staff_facuty_directory_app' div.
 *
 * @ingroup themeable
 */
#}
<svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1"
  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <symbol id="icon-triangle-down" viewBox="0 0 20 20">
      <title>triangle-down</title>
      <path d="M5 6h10l-5 9-5-9z"></path>
    </symbol>
    <symbol id="icon-triangle-up" viewBox="0 0 20 20">
      <title>triangle-up</title>
      <path d="M15 14h-10l5-9 5 9z"></path>
    </symbol>
    <symbol id="icon-phone" viewBox="0 0 32 32">
      <title>phone</title>
      <path
        d="M22 20c-2 2-2 4-4 4s-4-2-6-4-4-4-4-6 2-2 4-4-4-8-6-8-6 6-6 6c0 4 4.109 12.109 8 16s12 8 16 8c0 0 6-4 6-6s-6-8-8-6z">
      </path>
    </symbol>
    <symbol id="icon-address-book" viewBox="0 0 32 32">
      <title>address-book</title>
      <path
        d="M6 0v32h24v-32h-24zM18 8.010c2.203 0 3.99 1.786 3.99 3.99s-1.786 3.99-3.99 3.99-3.99-1.786-3.99-3.99 1.786-3.99 3.99-3.99v0zM24 24h-12v-2c0-2.209 1.791-4 4-4v0h4c2.209 0 4 1.791 4 4v2z">
      </path>
      <path d="M2 2h3v6h-3v-6z"></path>
      <path d="M2 10h3v6h-3v-6z"></path>
      <path d="M2 18h3v6h-3v-6z"></path>
      <path d="M2 26h3v6h-3v-6z"></path>
    </symbol>
    <symbol id="icon-envelop" viewBox="0 0 32 32">
      <title>envelop</title>
      <path
        d="M29 4h-26c-1.65 0-3 1.35-3 3v20c0 1.65 1.35 3 3 3h26c1.65 0 3-1.35 3-3v-20c0-1.65-1.35-3-3-3zM12.461 17.199l-8.461 6.59v-15.676l8.461 9.086zM5.512 8h20.976l-10.488 7.875-10.488-7.875zM12.79 17.553l3.21 3.447 3.21-3.447 6.58 8.447h-19.579l6.58-8.447zM19.539 17.199l8.461-9.086v15.676l-8.461-6.59z">
      </path>
    </symbol>
  </defs>
</svg>
<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="mta_staff_facuty_directory_app"></div>
```

Once we clear the cache we will no longer see anything on the page, but using the browser's inspect function we can confirm all is as it shoud be.

**Whoops** Nope, it's not there. Looks like we may need the hook_theme after all.

Yep, the minimal function

```php
function mta_staff_faculty_directory_theme() {
  return [
    'directory' => [],
  ];
}
```

is all we need to get it going.

I may refactor this in the future to use the Controller only to post this. probably by adding a getDirectoryMarkup() method which will return all of this HTML to the `#markup` member of the `content` array.

**Next Up** Bringing in the React part of the program and using Webpack to compile it.

### [Part 3 »](./DrupalReactModulePart3.md)
