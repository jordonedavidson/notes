# Building a Blog From the symfony/website-skeleton Project

As a developer I'm always looking to learn new things. The previous 10 years of my career was nearly exclusively in the [Lasso](https://www.lassosoft.com) world. During that time a lot of really cool technologies were invented that I simply did not have the time to investigate.

In my current position as the main Web Developer at [Mount Allison Universtiy](https://mta.ca) I have the opportunity to play catch-up and really get into what I've missed.

One of the things that I was able to learn about in the last couple of projects I worked on for my former employer was PHP's package manager [Composer](https://getcomposer.org/). Looking through all the available packages I became vaguely aware of something called Symfony. I didn't use it then, but it lodged in my brain.

One of my tasks at MtA was to modernise some of the internal tools we use in the department. I decided to move to a decoupled API/Frontend approach because I felt this would be more flexible moving forward. That decision prompted me to look into modern API developement tools (I'd written several by hand in the past, but had a hunch that there was an easier way.) This led me to the [Api-Platform](https://api-platform.com) project, which is based on the Symfony framework. It was both very quick to deploy and a huge learning curve to deploy correctly. This was my first real interaction with Symfony

We are currently undertaking a revamp of the MtA web presence which will change our CMS platform to [Drupal](https://www.drupal.org), which (as of version 8) is also based on the Symfony framework.

I'm starting to see a pattern. Looks like Imma gonna be learning Symfony.

I've spent the past several months delving into every YouTube tutorial for Symfony and Drupal that I can find. I'm able to follow along as they go, but I've found that I don't have very good retention. Taking notes helps, but my handrwriting is barely legible at times, so I spend as much time deciphering as I do understanding.

I came to the conclusion that taking notes in [Markdown](https://en.wikipedia.org/wiki/Markdown) files was the best way for me to capture what I'm learning. It's easy to type in, formatting is very quick, and, best of all, if handles code snippets with ease.

This blog is where I'll be posting the notes I've taken on eah technology that I'm learning. Primarily as a valuable resource for myself, and, I also hope that you might get some benefit from it as well.

Sometimes the posts will be as simple as the available options for a CLI command, other times there will be multip part series documenting larger scale projects. One of those will be the story of creating this blog, from the ground up staritng with the [symfony/website-skeleton](https://packagist.org/packages/symfony/website-skeleton) project.

I hope you'll get somthing from my experiences, if only to laugh every now and then.
