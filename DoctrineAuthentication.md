# Setting up authentication in Doctrine (API Platform)

**Goal:**

- To allow anonymous authentication for GET requests
- To require authentication for POST, PUT and DELETE requests
- Authentication handled by remote LDAP server.

This goal is complicated a little bit by the server (Apache 2.4) being locked down by default.

## Step 1: Allow non authenticated connections to the API

This particular server is not using virtual hosts. Individual "sites" are just different folders in the web root. `https://servername.ca/` serves from `/var/www/html` and the api `https://servername.ca/phonedb_api` serves from `/var/www/phonedb_api/public`

The server by default is locked down by LDAP (_ie_ the first rule for the \_default\_ host) so we need to open up the directory with its own rule.

```
<Directory "/var/www/phonedb_api/public">
        Allow from all
        AuthType None
        Options +FollowSymLinks
        AllowOverride all
        Require all granted
</Directory>
```

Now we can hit the api endpoint without authenticating.

## Step 2: Allow both authentication and anonymous access to the API endpoints

Api-Platform handles authentication in a couple of files.

- `/config/services.yaml` is where the LDAP service is defined.  
  The host, port, encryption and other options are defined as well as the adapter class. These settings are the base for all LDAP connections that will be set up (you can have as many as your use cases require.)
- `/services/packages/security.yaml` contains the ldap provider rules and the authentication handler for the firewall.

Setting up the LDAP over all is not fhe focus of this article. [This Article](https://symfony.com/doc/current/security/ldap.html) is what I used to get it going.

The 2 key entries in `/services/packages/security.yaml` for our purposes are:

1. In the `providers:my_ldap:ldap` section set `default_roles` to `[ROLE_USER]`  
   **_Note_** Any other role will work here, as long as they are set up in the `role_hierarchy` section of this file
2. Ensuring that the `firewalls:main` entry contains the following configurations to allow both anonymous and LDAP authenticated connections:

```yaml
firewalls:
  main:
    pattern: ^/api
    anonymous: true

    http_basic_ldap:
      service: Symfony\Component\Ldap\Ldap
      dn_string: "<your dn string>"
      query_string: "<your query string>"
```

At this point we can just go ahead connect to the API endpoint (the `pattern` above) and we can use basic authentication to connect with our LDAP credentials.

## Step 3: Require authentication for POST, PUT and DELETE requests.

The data this API provides is pretty much publicly available so it's fine to allow anonymous requests for retreival. However only certain users have authority to update it. So we need to lock that down.

This is accomplished in the annotations for each Entity file (the Doctrine representations of the database schema). These live in the `/src/Entity` folder.

Each entity uses the @ApiResource annotation to expose itself to the API. In Api-Platform there are 2 types of [operations](https://api-platform.com/docs/core/operations/#operations) that can be performed on an entity:

- **Collection Operations**
  - GET retreives the list of elements
  - POST creates a new element
- **Item Operations**
  - GET retrieves one element
  - PUT updates an element
  - DELETE removes one element.

By adding these objects to the @ApiResource we can control what users can access what.

**IMPORTANT** If you add one of these you have to detail the accessability of the GET request. It is required.

We want anyone to be able to GET but only authenticated users to be able to manipulate the data. Here's what that looks like:

```php
/**
* @ApiResource(
*  collectionOperations={
*    "get",
*    "post"={
*      "access_control"="is_granted('ROLE_USER')",
*      "access_control_message"="Must be authenticated to add Buildings."
*    }
*  },
*  itemOperations={
*    "get",
*    "put"={"access_control"="is_granted('ROLE_USER')"},
*    "delete"={"access_control"="is_granted('ROLE_USER')"}
*  }
* )
* ...
*/
```

In both the `collectionOperations` and `itemOperations` sections get is wide open (no accessability object attached) so anonymous users can query the API for this entity.

The post, put and delete request types require and authenticated user with the `ROLE_USER` permission. This is the role that was set in the security providers section in [Step 2](#step-2-allow-both-authentication-and-anonymous-access-to-the-api-endpoints).

##Step 4: Clear the cache.
Symfony 4 is a pretty cool framework, and it's pretty quick too. However a lot of its speed comes from caching. In this case the Entity caching is very important. The updated entity annotations (and maybe the routing as well) won't kick in until the cache is cleared. Do this with

```console
php ./bin/console cache:clear
```

from the root directory of the app. (`/var/www/phonedb_api/public` in my case.)

## Conclusion

It took about 3 hours of monkeying around and reading various bits of (slightly cryptic) documentation to figure this out. Now it works exactly as I hoped. For simple data requests I no longer need to potentially expose a legit (and powerful) username/password combo in any apps that I write to hit this data. And the users of the Administraion application are required to have a priviliged login to mke changes to the underlying data.

**Succès** 🍻

## Postscript

Once I connected a live app to this API I ran into the dreaded CORS error:

```
No 'Access-Control-Allow-Origin' header is present on the requested resource.
```

Ultimately I discovered that this is controlled by the Symfony routing package `nelmio/cors-bundle` which is installed by default with api-platform.

The config file for this package is `config/packages/melmio_cors.yaml` and is pretty straightforward.

```yaml
nelmio_cors:
  defaults:
    origin_regex: true
    allow_origin: ["%env(CORS_ALLOW_ORIGIN)%"]
    allow_methods: ["GET", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
    allow_headers: ["Content-Type", "Authorization"]
    expose_headers: ["Link"]
    max_age: 3600
  paths:
    "^/": ~
```

Clearly this file gives a lot of flexibility to what the API will serve and to whom. The key thing to note is the `allow_origin` default which references the environment variable `CORS_ALLOW_ORIGIN`.

All I needed to do was set this value in the `.env.local` file in the production API and my app recieved the data as desired.
