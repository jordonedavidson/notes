# Building a Drupal Module with Embedded React

## Part 1

**Overall Goals:**

- Use the [`drush generate` command](./DrushGenerate.md) to scaffold a basic drupal module.
- Use [Webpack](./Webpack.md) to manage compiling the React/ES6 code into a JS library.
- Connect the compiled React app to the Drupal module.
- Have the React module appear as a page in a Drupal site.

## Step 1: Scaffold the Basic Module

The goal of the module is to produce a page on the site called `/staff-faculty-directory` so our basic module will require:

- \*.info.yml _Required for all modules_
- \*.libraries.yml _To point to the custom js and css files_
- \*.routing.yml _To set the routing for the page_
- A Controller _For the display_
- /templates/\*.html.twig _To hold the basic markup for the page_

The name of the module will be MtA Staff/Faculty Directory (I'm curious to see what the resulting machine name will be for that.) And it will live in the `/docroot/modules/custom/` directory.

Use the command `drush generate module-standard` to generate the necessary files. Here is the result:

```console
 Welcome to module-standard generator!
–––––––––––––––––––––––––––––––––––––––

 Module name:
 ➤ MtA Staff/Faculty Directory

 Module machine name [mta_staff_faculty_directory]:
 ➤

 Module description [The description.]:
 ➤ React based, filterable display for the Mount Allison Staf/Faculty Phone Directory

 Package [Custom]:
 ➤

 Dependencies (comma separated):
 ➤

 Would you like to create install file? [Yes]:
 ➤ no

 Would you like to create libraries.yml file? [Yes]:
 ➤

 Would you like to create permissions.yml file? [Yes]:
 ➤ no

 Would you like to create event subscriber? [Yes]:
 ➤ no

 Would you like to create block plugin? [Yes]:
 ➤ no

 Would you like to create a controller? [Yes]:
 ➤

 Would you like to create settings form? [Yes]:
 ➤ no

 The following directories and files have been created or updated:
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 • modules/custom/mta_staff_faculty_directory/mta_staff_faculty_directory.info.yml
 • modules/custom/mta_staff_faculty_directory/mta_staff_faculty_directory.libraries.yml
 • modules/custom/mta_staff_faculty_directory/mta_staff_faculty_directory.module
 • modules/custom/mta_staff_faculty_directory/mta_staff_faculty_directory.routing.yml
 • modules/custom/mta_staff_faculty_directory/src/Controller/MtaStaffFacultyDirectoryController.php
```

Well that was pretty easy. It even created the basic routing file, but not the twig template.

Let's activate it and see what happens out of the box. The default route that was activated was `/mta-staff-faculty-directory/example` so we'll hit that.

![Default Install](./images/staff_dir_default_install.png)

Looks like it did what we wanted. Now we can work on customising it.

### Update the route.

In the `mta_staff_faculty_directory.routing.yml` file update the route to point to the page we want.

```yaml
mta_staff_faculty_directory:
  path: "/staff-faculty-directory"
  defaults:
    _controller: '\Drupal\mta_staff_faculty_directory\Controller\MtaStaffFacultyDirectoryController::build'
  requirements:
    _permission: "access content"
```

**What Changed**

We have removed the .example from the block heading, changed to path to `staff-faculty-directory` and removed the `_title` default since the react app will handle that.

**Important** Nothing will change on the Drupal site until you run

```console
./vendor/bin/drush cr
```

to clear the cache. Drupal (and Symfony which it is based on) rely heavily on caching for speed of delivery. In development you become accustomed to using the `drush cr` command all...the...time...

**Next Up** Creating the template file and connecting it to the module.

### [Part 2 »](./DrupalReactModulePart2.md)
