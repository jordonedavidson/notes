# Creating a Blog from Scratch Using Symfony 4

## Part 10: Submitting a Post

**Goals**

- Update the form to pass the username
- Update the handler to get the correct User object.
- Consider CSRF protection on the form.
- Look at ways to protect the form submission page
- Successfully submit a post.

## Pass the Username

In the login form we saw the `app.user` object. When a user is authenticated this globally accessable object contains the user information. For our purpose it contains the username field.

We can add that to the form with a hidden input like this:

```twig
<input type="hidden" name="username" value="{{ app.user.username }}" />
```

However if there is no logged in user, accessing this object will throw an error. So we need to test for it with an `{% if app.user %}{% endif %}` block.

Wrapping that around the form will ensure that it only displays to logged in users.

However that visitor will need to see something, so we'll add a message and a login button for them. Remember twig doesn't have an `else` statement so we test on the negation of the condition.

```twig
{% if not app.user %}
  <h2>You need to be logged in to add a note</h2>
  <a href="{{ path('app_login')}}" class="btn btn-primary" title="Login">Login</a>
{% endif %}
```

The twig `path()` function will provide the current URL path for the `app_login` route. This method of handling internal links is preferable because it doesn't tie us down to a particular page structure.

> We could assume that if you get to this page that you are logged in by putting a handler in the route that redirects to the app_login page for non-authenticated visitors. And I may do that in the future.

## Handle the Form

Now that we have a username field we need to use it to get the User object required by the Post entity.

We can accomplish that in basically the same way as the `LoginFormAuthenticator` did.

```php
# src/Controller/PostController.php

...
use App\Entity\User;
use App\Repository\UserRepository;

class PostController extends AbstractController
{
  ...
  /**
   * @Route("post/add", name="add_post", methods={"GET", "POST"})
   *
   * @param \Symfony\Component\HttpFoundation\Request $request The parameters from the POST.
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function addNewPost(Request $request, UserRepository $userRepository): Response
  {

      $post = new Post();
      $form = $this->createForm(PostFormType::class, $post);
      $form->handleRequest($request);

      # The POST response
      if ($form->isSubmitted() && $form->isValid()) {
          $user = new User();
          $username = $request->request->get('username');

          $user = $userRepository->findOneBy(['username' => $username]);
          try {
              # Make sure we have a user.
              if (!$user) {
                  // fail authentication with a custom error
                  throw new \Exception('Invalid Author.');
              }

              $entityManager = $this->getDoctrine()->getManager();
              $entityManager->persist($post);
              $entityManager->flush();

              return $this->redirect($this->generateUrl('post', ['slug' => $post->getSlug()]), 303);
          } catch (\Exception $e) {
              return $this->redirectToRoute('app_login', ['error' => $e->getMessage], 401);
          }
      }

      # The GET response
      return $this->render('post/add.html.twig', [
          'postForm' => $form->createView(),
      ]);
  }
}
```

We are going to need access to the User entity and its repository so we add those at the top.

Inside the POST response block we grab the username from the form submission and use the dependency-injected `$userRepository` to retrieve the associated record.

If that request failed `$user` will be `NULL` so we throw an exception that will be used to redirect to the login page with the associated error message.

Otherwise we add the user to the `$post` object with the `setAuthor()` method, and save the post to the database.

Finally we redirect the user to the new post.

## Trying It Out

I'm going to add the post `New Note` with the body

```
# New Note

This is a new note
```

> The body of the posts will all be in Markdown syntax

I mark it published. (So that it will show up in the posts listing.), Set the date (eventually that will be automatic) and Submit.

![New Note](./images/NewNote.png)

**🎉 Success!! 🎉**

## Future Improvements

Now We have a functioning (bare bones as it is) Blog platform. However before it can go live there are serveral improvements that need to be made.

1. Currently anybody can register and add content. That needs to be locked down
2. Different user roles need to be introduced. Author, Admin, User (for comments perhaps) _etc._
3. Better handling of improper users attempting to get to the add page.
4. Need a way to edit and delete posts.
5. Need to handle the publishing flag better.
6. Translate Markdown to HTML
7. Full facelift. This site is **Ugly** with a capital Ug.
8. Tests for the new components.

However, this project has come a long way, so let's take a moment to be proud of what was accomplished.

## Next Steps.

Markdown to HTML, with caching, (this is actually pretty easy.) Adding some form validation and defaults to the fields. Adding an Edit post route. And, of course more Tests.

I may not document all of the steps for each of these areas. But when it comes to doing the look and feel with CSS-Grid I'll be taking careful notes.

Thanks for hanging out with me through this process.

### [<< Step 9 - User Login](SymfonyBlogPart9.md) | [Step 11 - Translating Markdown >>](SymfonyBlogPart11.md)
