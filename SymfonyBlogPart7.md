# Creating a Blog from Scratch Using Symfony 4

## Part 7: User Registsration

**Goals**

- Create the process for registering a user to the site
- User Registration handler
- User Registration page
- Registration Form input with validation

You may be asking yourself, "If this is just a personal blog, why does it need a registration process?" Good question. My answer has 2 parts.

1. In order to add content to the site I need to be an author. This is the simplest way to add that user to the site.
2. This is also a learning experience. Pretty much all applications need some form of user registration and authentication so I may as well learn how now.

Besides if I ever want to let anyone else add content to the site I'll need a conveient way to add their user account.

## What We Need

Obviously user registration will be making use of the **User** entity, so our code will need to interact with that.

We'll need to have a page where the user registers, so we need a **Controller** with a **Route** and a **Template**.

Also there will be a Form to fill out so we need to be able to create one of those on the template and we also need to be able to **Process** and **Validate** the form submission.

Wow, that's a lot of code we need to create. Better buckle up for a long ride.

## The Cheat

Fortunately Symfony (like most frameworks) is there to make our life easier. Just like there were simple intreactive shell commands to make the Controllers and Entities, there is also a proscess to quickly set up user registration for the site.

> The latest documentation provided by Symfony is found in this article: [How to Implement a Registration Form](https://symfony.com/doc/current/doctrine/registration_form.html). I'm basically going to repeat that here, but that link should always have the most up-to-date documentation.

All we need to do is run the command:

```console
./bin/console make:registration-form
```

Once we answer all the questions based on our setup we end up with:

- `src/Controller/RegistrationController.php` That handles the `/register` route
- `templates/registration/reigister.html.twig` To display the registration page
- `src/Form/RegistrationFormType.php` To handle the design and constraints of the registration form

> In Symfony Form classes always end with 'Type' by convention

Here's What we ended up with:

## RegistrationFormType

This builds out the fields of the form with their constraints (for validation) and connects it to the User class.

```php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationFormType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('username')
      ->add('plainPassword', PasswordType::class, [
        // instead of being set onto the object directly,
        // this is read and encoded in the controller
        'mapped' => false,
        'constraints' => [
          new NotBlank([
             'message' => 'Please enter a password',
          ]),
          new Length([
            'min' => 6,
            'minMessage' => 'Your password should be at least {{ limit }} characters',
            // max length allowed by Symfony for security reasons
            'max' => 4096,
          ]),
        ],
      ])
      ->add('agreeTerms', CheckboxType::class, [
        'mapped' => false,
        'constraints' => [
          new IsTrue([
            'message' => 'You should agree to our terms.',
          ]),
        ],
      ])
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => User::class,
    ]);
  }
}
```

## RegistrationController

Builds the form for the template, and handles both the display and submission of the form. After the new user is successfully created it redirects to the Site home page.

```php
namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
```

## resgister.html.twig

This template renders the form. Note the form is drwan with a series of function calls.

```twig
{% extends 'base.html.twig' %}

{% block title %}Register{% endblock %}

{% block body %}
  <h1>Register</h1>

  {{ form_start(registrationForm) }}
    {{ form_row(registrationForm.username) }}
    {{ form_row(registrationForm.plainPassword) }}
    {{ form_row(registrationForm.agreeTerms) }}

    <button class="btn">Register</button>
  {{ form_end(registrationForm) }}
{% endblock %}
```

## What Does it Look Like

Well, pretty plain:

![Registration Form](./images/RegistrationForm.png)

But does it work?

![Validation](./images/RegisterValidation.png)

Well I guess I'd better agree to the terms (whatever they are!)

![Exception](./images/RegisterException.png)

**Whoops** I guess I need to do some work after all.

## Tweaking the Registration Process.

When I created the user entity I used this declaration:

```php
/**
 * @ORM\Column(type="string", length=100)
 */
private $realname;
```

This is one of those cases where the unstated can bite you. Looking at that you wouldn't know that this declaration implys that this field must exist. In Doctrineland that is the default. In order for a field to be optional im must be declared with the parameter `nullable=true`.

So I have two choices, I can change the entity and migrate the database to update the field. Or I can go along with what I originally intended and modify the form to add the Real Name.

Obviously I'm going with option 2.

### Adding the realname Field to the Form

I'll want the user to enter their real name after the password field. so I'll put a new `add` method in that spot on the `$builder` object in the `RegistrationFormType::buildForm() method

I want the field to be a text field (which is the default) and I want to set the label to "Full Name". To do this I need to explicitly call in the `TextType` class in a `use` statement and then I'll insert the `add` method.

```php
 // ...
 use Symfony\Component\Form\Extension\Core\Type\TextType;
 // ...

$builder
  // ...
  ->add('realname', TextType::class, [
        'label' => 'Full Name'
    ])
```

Before I forget, I need to add the `home_page` route to the return statement in the RegistrationController.

```php
return $this->redirectToRoute('home_page');
```

Now I call in the display of the field into the template with this:

```twig
{{ form_row(registrationForm.realname) }}
```

after the plainPassword field

Fingers Crossed...

## It Worked.

The site successfully redirected to the home page and, using an other cool feature of Doctrine we can see that the record was added to the database.

```console
> ./bin/console doctrine:query:sql 'SELECT * FROM user WHERE username ="jordon"'
array(1) {
  [0]=>
  array(8) {
    ["id"]=>
    string(2) "10"
    ["username"]=>
    string(6) "jordon"
    ["roles"]=>
    string(2) "[]"
    ["password"]=>
    string(60) "$2y$13$uSFrJRXvfvQCa09XxBDWme2sF217fxWsD4CFSaWyTsnh.UJjm7sHO"
    ["realname"]=>
    string(15) "Jordon Davidson"
    ["twitter"]=>
    NULL
    ["github"]=>
    NULL
    ["website"]=>
    NULL
  }
}
```

That password sure is something to behold. The RegistrationController did that in the submission handler using Symfony's `UserPasswordEncoderInterface`. It looks pretty secure to me.

## What About TDD

Since everything here is out of the box Symfony I'm going to let the testing slide on this one. I'll get to writing tests for it when I want to extend to adding more users. And for setting up different roles.

## Next Steps.

It would be nice to add a post from the web interface, so that's up next. After I'll look at user authentication (spoiler: there's a groovy `make:auth` function to help us with this one), and roles to tighten up the site.

### [<< Step 6 - Blog Post Test Part 3](SymfonyBlogPart6.md) | [Step 8 - Making Posts >>](SymfonyBlogPart8.md)