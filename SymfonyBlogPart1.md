# Creating a Blog from Scratch Using Symfony 4

## Part 1: Database Setup

Since I'm going to be using the Symfony `console` command for the entire process of making Entities (the descriptions of database tables and their getter and setter functions) and Migrations (writing database structure changes to MySQL) the first thing I need to do is tell symphony how to connect to the database. This is taken care of in the `.env` file

> **A note on the .env system:** Symfony makes extensive use of special hidden files to hold the values of system-wide Environment Variables. These are held in a number of files that all begin with .env The most important are `.env`, `.env.local` and `.env.test`.

> In general you should **never** commit .env style files to version control because they often contain sensitive data like passwords. In Symfony it is expected that you will commit `.env` and `.env.test` using the `.local` versions of these files to override any sensitive data. The `.local` versions of the files are **not** to be committed to version control. It's up to you to figure out how to deploy and maintain the .local versions on the production server.

The default `.env` file contains a lot of good information, including how to connect to the mysql database.

```yaml
###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://db_user_:db_pass$@127.0.0.1:3306/db_name
###< doctrine/doctrine-bundle ###
```

It's ok to put in the data for the local database user and commit it because our development machine is not connected to the world. And we'd never use the same credentials on a production server as we do locally. Right? 😉

In mysql I create the database `tech_notes` and a user to admin it.

## Entities

Entities are classes that are used to work with the database table of the same name (`src/Entity/Post.php` class connects to the `post` table in the db.).

The member variables match up with the table fields and the class methods are (minimally) the getters and setters for those fields.

I need a user table so that I can authenticate (don't want just anyone writing posts on my blog!) and there is a `make:user` command. This function helps me set up the User entity and a few other files.

```console
> ./bin/console make:user

 The name of the security user class (e.g. User) [User]:
 >

 Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
 >

 Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
 > username

 Will this app need to hash/check user passwords? Choose No if passwords are not needed or will be checked/hashed by some other system (e.g. a single sign-on server).

 Does this app need to hash/check user passwords? (yes/no) [yes]:
 >

 created: src/Entity/User.php
 created: src/Repository/UserRepository.php
 updated: src/Entity/User.php
 updated: config/packages/security.yaml


 **Success!**


 Next Steps:
   - Review the new App\Entity\User class.
   - Use make:entity to add more fields to the User entity and then run make:migration.
   - Create a way to authenticate! See [https://symfony.com/doc/current/security.html](https://symfony.com/doc/current/security.html)
```

I picked the defaults for the User class name, to store the data locally with Doctrine, and to do the password verification locally (I'm already learning enough new concepts without adding single-sign-on headaches.) I decided to use `username` instead of email as the "display" name in case that connected to a view somewhere.

Now I have a User entity, and User repository (The repository handles searches on the table.) and my `security.yaml` file has been updated to use the username for authentication.

```yaml
#config/packages/security.yaml
security:
  encoders:
    App\Entity\User:
      algorithm: auto

  # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
  providers:
    # used to reload user from session & other features (e.g. switch_user)
    app_user_provider:
      entity:
        class: App\Entity\User
        property: username
```

That will get used later.

The screen said to review the `App\Entity\User` class and make any changes before _Migrating_ the entity to the db. As of now the database is still empty.

> There is a lot to unpack in the Entity file, so I'm going to leave that for another post. If you want to understand more right now check out Symfony's [Doctirne documentation](https://symfony.com/doc/current/doctrine.html)

Currently there are 4 fields described: id, username, roles, and password. The PHPDoc blocks contain annotaions which describe the types of fields they are.

This will do for now, I can always update the fields later.

To save the table to that database I'll run `console make:migration`

```console
> ./bin/console make:migration

  Success!

 Next: Review the new migration "src/Migrations/Version20190820190041.php"
 Then: Run the migration with php bin/console doctrine:migrations:migrate
 See https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
```

This created a new Migration class that can be used in the future to roll back to if we need. On deployment all the migrations can be run in sequence to get the db structure to the correct state.

Running the `doctrine:migrations:migrate` will finally save the table to the database.

```console
> ./bin/console doctrine:migrations:migrate

                    Application Migrations

WARNING! You are about to execute a database migration that could result in schema changes and data loss. Are you sure you wish to continue? (y/n)y
Migrating up to 20190820190041 from 0

  ++ migrating 20190820190041

     -> CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB

  ++ migrated (took 44.9ms, used 16M memory)

  ------------------------

  ++ finished in 48.1ms
  ++ used 16M memory
  ++ 1 migrations executed
  ++ 1 sql queries
```

Finally the table exists. And, for good measure, we have a migration_versions table that keeps track of all the migrations that get created and run.

## Next Steps.

More DB table creation and finally our first test and route.

## [<< Intro](SymfonyBlogIntro.md) | [Part 2 - More Tables and the First Route](SymfonyBlogPart2.md)
