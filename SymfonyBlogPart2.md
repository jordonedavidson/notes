# Creating a Blog from Scratch Using Symfony 4

## Part 2: More Tables and the First Route

With the User entity taken care of now it's time to add a couple more tables/entities to the blog. One for Posts and one for Tags. To create these I'll use the `console make:entity` command and follow the prompts.

For the Post entity we'll add the following fields:

- title: The post title
- slug: The user-friendly link for the post
- abstract: Short text blurb for use in post listings (optional)
- body: The body of the post
- created_date: When the post was written
- published: Published status of the post.

The Tag entity is much simpler

- description: The text of the tag for use in posts

### Creating Post and Tag

The `make:entity` process is very user-friendly for a command line interface. It prompts you with default options, offers a help screen for field types and even a wizard for structuring field relationships to other entities.

Here's a look at the options for a field, (pulled straight from the command line, yes, even the wizard emoji):

**Main types**

- string
- text
- boolean
- integer (or smallint, bigint)
- float

**Relationships / Associations**

- relation (a wizard 🧙 will help you build the relation)
- ManyToOne
- OneToMany
- ManyToMany
- OneToOne

**Array/Object Types**

- array (or simple_array)
- json
- object
- binary
- blob

**Date/Time Types**

- datetime (or datetime_immutable)
- datetimetz (or datetimetz_immutable)
- date (or date_immutable)
- time (or time_immutable)
- dateinterval

**Other Types**

- json_array
- decimal
- guid

> **A brief word about database relationships.** Intra-table relationships are kind-of the _raison d'être_ of "Relational Database Systems" like MySQL. The idea is that a field in one table forms a relationship to another table. For instance an `author` field in the `post` table would have a one-to-one relationship with the `user` table. This gives you access to all of the author's information from the post and, conversly, lets you easily access all of the posts made by an author. For more information on the topic here is a [decent article](https://database.guide/the-3-types-of-relationships-in-database-design/).

Speaking of relationsips. Doctrine is happy to help you create these, however the Entity you want to relate to needs to exist first, If you make a mistake and do them out of order, don't worry. Just skip that field, make the entity you need, then come back and add the relationship field to the first one. The `make:entity` command is also for updating existing entities.

Again, the `make:entity` process has a helper to explain this, the relationship wizard. Here is its explantion of the possible relationships between the Tag.tag field and the Post entiity.

| Type       | Description                                                                                                        |
| ---------- | ------------------------------------------------------------------------------------------------------------------ |
| ManyToOne  | Each Tag relates to (has) one Post. Each Post can relate to (can have) many Tag objects                            |
| OneToMany  | Each Tag can relate to (can have) many Post objects. Each Post relates to (has) one Tag                            |
| ManyToMany | Each Tag can relate to (can have) many Post objects. Each Post can also relate to (can also have) many Tag objects |
| OneToOne   | Each Tag relates to (has) exactly one Post. Each Post also relates to (has) exactly one Tag.                       |

Our tables have two relationships:

1. A many-to-one relationship from Post.author to the User entity
2. A many-to-many relationship from Tag.tag to the Post entity.

Which means that each post has an unique author (many-to-one) and that each tag can appear in as multiple posts and a post can have multiple tags (many-to-many).

Interestingly the User.post field and the Post.tag fields are not generated when creating those entities. Doctrine adds them retoactively when we set up the relationship fields Post.author and Tag.tag. Pretty slick!

### Updating Entities

Currently the User entity has 5 fields described: id, username, roles, password and post.

This is a good start. but I'd like to add some extra fields: realname, twitter, github, and website. To do that again I'll use the console's `make:entity` command. (Remember make:entity handles both the initial creation and adding new fields to existing entities.)

### Migrations and The Code

Before we run the migraions let's take a look at the generated code for the new entities.

Whoa, That's a lot of code! And we didn't write a lick of it.

The Entity class code is easily broken down into 3 sections

1. Variables that represent the fields in the table
2. Getter and Setter methods for each of the variables
3. Optional additional methods for custom tasks performed by or on the entity. User::getSalt() and User::eraseCredentials() for example.

Here's a look at the simplest entity, Tag.

```php
#/src/Entity/Tag.php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Post", inversedBy="tags")
     */
    private $tag;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $description;

    public function __construct()
    {
        $this->tag = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Post[]
     */
    public function getTag(): Collection
    {
        return $this->tag;
    }

    public function addTag(Post $tag): self
    {
        if (!$this->tag->contains($tag)) {
            $this->tag[] = $tag;
        }

        return $this;
    }

    public function removeTag(Post $tag): self
    {
        if ($this->tag->contains($tag)) {
            $this->tag->removeElement($tag);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}

```

Overall this looks like a pretty standard PHP class. You may have noticed two interesting things in the code:

First there is an id variable that we didn't add when we defined the Entity. And there are some strange entries in the DocBlocks for the variables.

Doctrine takes care of adding the unique key id field for all tables, so you don't have to. Nice.

The odd DocBlock comments are part of Doctrine's [Annotations system](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/annotations-reference.html). This is where the system manages the structure of each field in the database. Fortunately it is pretty easy to see what's going on.

Once we are happy with the Entities we can make and run the database migration with

```console
./bin/console make:migration
./bin/console doctrine:migrations:migrate
```

After processing the migration we have the following tables in the `tech_notes` database.

| Table                  | Description                                                                       |
| ---------------------- | --------------------------------------------------------------------------------- |
| **migration_versions** | Existing, Holds database migration data                                           |
| **post**               | New, Data for the blog posts                                                      |
| **tag**                | New, Tags for posts                                                               |
| **tag_post**           | New, Link table for tags and posts                                                |
| **user**               | Updated, Now with an author field keyed to post and the new text fields we added. |

## Next Steps

Ok, next time for sure we will create a _test_ for a _controller_ that handles our first _route_. Look at all that technical jargon!

## [<< Part 1 - Database Setup](SymfonyBlogPart1.md) | [Part 3 - The First Route >>](SymfonyBlogPart3.md)
