# Understanding the loader path

## Flow of the main class Yms_Wc_Timed_Discount_Code

1. The constructor sets the version and plugin name then:
    1. Loads Dependancies: this requires the clases:
        - Yms_Wc_Timed_Discount_Code_Loader
        - The translation class
        - Yms_Wc_Timed_Discount_Code_Admin
        - Yms_Wc_Timed_Discount_Code_Public  
        and instantiates the Loader class in `$this->loader`
    2. Sets the locale (for use with translation)
    3. Loads admin hooks using `define_admin_hooks`
    4. Loads public hooks using `define_public_hooks`
2. Inside the `define_x_hooks` methods the appropriate class is instantiated then individual hooks are registered using the loader class' `add_action` method.  
The hooks must be individually listed here, there is no automatic seek and find.
3. The loader's `add_action` method adds:
    - the hook type: *ex.* wp_enqueue_scripts,
    - the class where the callback function (method) exists: *ex.* Yms_Wc_Timed_Discount_Code_Public
    - the method name of the callback function (method): *ex.* enqueue_scripts
    - *Optional* integer priority of the hook: Default 10
    - *Optional* integer number of accepted arguments passed to the callback function: Default 1

## How to register shortcodes in this manner

At first blush it looks like I have to duplicate the add_hooks functionality to use the `add_shortcode()` function. However, upon some basic research I found that it can be done within the existing hooks setup by utilising the admin `init` hook as follows:

### In the Yms_Wc_Timed_Discount_Code::define_admin_hooks() method

```php
private function define_public_hooks() {

    $plugin_public = new Yms_Wc_Timed_Discount_Code_Admin( $this->get_plugin_name(), $this->get_version() );

    $this->loader->add_action( 'init', $plugin_admin, 'register_shortcodes');
}
```

### In Yms_Wc_Timed_Discount_Code_Admin

```php
public function register_shortcodes(){

    add_shortcode('shortcode_name', array($this, 'shortcode_method'));

}
```

All of my shortcode functionality will live in Yms_Wc_Timed_Discount_Code_Admin::shortcode_method(). Which, I suppose, could get even sillier by calling in methods from a dedicated shortcode class, for maximum obtuse abstraction.
