# Building a Drupal Module with Embedded React

## Part 4: Pulling in Our Script via the Library

**Goals**

- include `/dist/main.js` as a library for the module
- see it work!

## Updating libraries.yml

CSS and Javascript assets that are particular to a Drupal module are included via entries in the `MODULE_libraries.yml` file.

> *MODULE* is replaced by the module name, mta_staff_faculty_directory in our case.

If we take a look at the generated `mta_staff_faculty_directory.libraries.yml` file we see that it is already quite full. It already contains an entry for our module as well as entries for third party libraries jquery-labelauty and vuejs. Since this module is not using either of those we can safely remove them.

That leaves the base library entry for the module:

```yaml
# Custom module library for general purposes.
mta_staff_faculty_directory:
  js:
    js/mta-staff-faculty-directory.js: {}
  css:
    component:
      css/mta-staff-faculty-directory.css: {}
  dependencies:
    - core/drupalSettings
    - mta_staff_faculty_directory/jquery-labelauty
```

Again we aren't using jquery-labelauty so that line can go. However we know that bootstrap wants jQuery to be available so we'll replace that dependency with `core/jquery`

Our app didn't generate any custom css files so we can safely remove that section as well. Which leaves us with:

```yaml
mta_staff_faculty_directory:
  js:
    js/mta-staff-faculty-directory.js: {}
  dependencies:
    - core/drupalSettings
    - core/jquery
```

We left our bundled js file (`main.js`) in the `/dist` directory. So we have two choices:

1. Go back into `webpack.config.js`, import the `path` function and use it inside an `output` node to make our bundle match the existing route. Or
2. Modify the file to match what we have.

The correct answer is (1) but we are going to do (2) for now. I'll show the modifications we need to make to webpack config later.

Now our libraries.yml looks like

```yaml
mta_staff_faculty_directory:
  js:
    dist/main.js: {}
  dependencies:
    - core/drupalSettings
    - core/jquery
```

## Using the Library in the Module

Now that we have a hook to the js file we need to include it in the rendering of the module. To do this we modify the array in the *build()* method of the controller to add the reference.

The render array (\$build) ends up like this:

```php
$build['content'] = [
  '#theme' => 'directory',
  '#attached' => [
    'library' => [
      'mta_staff_faculty_directory/mta_staff_faculty_directory',
    ],
  ],
];
```

All that is left to do is clear the cache and see if it works.

**_Note:_** We've been working in the module directory for a while. To run drush either cd to the Drupal root directory or run

```console
../../../../vendor/bin/drush cr
```

from the module directory.

## Gotchas!

Well nothing could be that easy eh?

As I mentioned earlier, the original React App that I've puilled in was created using the excellent `create-react-app` package, which handles all of the build requirements behind the scenes. That's great for efficient standalone app creation, not so good when you want to "roll your own".

The console complains that the function `regeneratorRuntime` is not defined. Ok, some quick goolgling and it seems that we need to add a plugin to babel like so:

```console
npm install --save-dev @babel/plugin-transform-runtime
```

and in the babel area of webpack.config

```javascript
"babel": {
  "presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ],
  "plugins": [
    "@babel/plugin-transform-runtime"
  ]
},
```

Recompiling and cache clearing 🤞

And it renders!

## Final Thoughts and Cleanup.

### One Step Building and Cache Clearing

When building from scratch or troubeshooting you'll find yourself building and clearing the cache a lot. We can update the scripts to make that a one step process.

First we need to add the package `npm-run-all` to our dev dependencies

```console
npm install npm-run-all --save-dev
```

then we modify the scripts to define separate scripts for webpack and clearing the Drupal cache. Then we modify the build script to run them in sequence

```json
"scripts": {
  "webpack": "./node_modules/.bin/webpack",
  "clear:cache": "../../../../vendor/bin/drush cr",
  "build": "run-s webpack clear:cache"
}
```

Now `npm run build` handles both the bundling and cache clearing.
