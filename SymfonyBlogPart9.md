# Creating a Blog from Scratch Using Symfony 4

## Part 9: User Login

**Goals**

- Create a login page for Authors

## Logging in the Author

The app currently has a way to register a user, but no way to log them in. It's time to fix that.

Symfony provides a quick-n-easy way to implement login security via the `console make:auth` command. The official docs have a great breakdown on [implementing a login form](https://symfony.com/doc/current/security/form_login_setup.html) using this command. I'll be doing pretty much the same thing here.

## Generating the Files

The `make:auth` process will ask for some information as it goes. I'm going to go with a login form authenticator. I'll also be providing the authenticator class, security controller class, and I'll also ask for a `/logout` url.

```console
> ./bin/console make:auth

 What style of authentication do you want? [Empty authenticator]:
  [0] Empty authenticator
  [1] Login form authenticator
 > 1

 The class name of the authenticator to create (e.g. AppCustomAuthenticator):
 > LoginFormAuthenticator

 Choose a name for the controller class (e.g. SecurityController) [SecurityController]:
 >

 Do you want to generate a '/logout' URL? (yes/no) [yes]:
 >

 created: src/Security/LoginFormAuthenticator.php
 updated: config/packages/security.yaml
 created: src/Controller/SecurityController.php
 created: templates/security/login.html.twig

  Success!

 Next:
 - Customize your new authenticator.
 - Finish the redirect "TODO" in the App\Security\LoginFormAuthenticator::onAuthenticationSuccess() method.
 - Review & adapt the login template: templates/security/login.html.twig.
```

A quick check on `console debug:router` shows the new routes:

```console
  app_login   ANY      ANY      ANY    /login
  app_logout  ANY      ANY      ANY    /logout
```

Before I look at all the new things that have been installed. I'm going to do the "TODO" in `App\Security\LoginFormAuthenticator::onAuthenticationSuccess()` and give a redirect to the home page. In the future this will probably go to a dashboard, or to the previously requested page.

This is what the `make:auth` command provided:

```php
# Security/LoginFormAuthenticator.php

...
public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
{
  if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
    return new RedirectResponse($targetPath);
  }

  // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
  throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
}
```

All I need to do is use the suggested code in the comment with the route to the home page and remove the exception line.

```php
return new RedirectResponse($this->urlGenerator->generate('home_page'));
```

Testing out the form at `/login` with the user I created before sends me to the home page. When I check the Symfony profiler bar at the bottom of the page I see that I'm now logged in as "jordon"

![Authenticated](./images/Authenticated.png)

## Looking at What was Added

The `make:auth` process added 3 files to the app and updated another one. Let's take a look at what changed.

### Security/LoginFormController.php

The documentation refers to this code as the **Guard Authenticator**. Its job is to process the authentication request and only allow legitimate users into the app. _i.e._ it "Guards" the application by "authenticating" the credentials of the user that wants access.

There are several methods in this class, and there is some **Symfony Magic™** that goes on behind the scenes that calls them in the correct sequence. However it does that isn't important right now, let's just understand what each one is doing.

#### \_construct()

The class constructor is typical. It is called with a number of Dependency Injected parameters which it uses to set the member variables of the class.

#### supports()

This method simply tests to see if the form was POSTed from the `app_login` route. if it wasn't the authentication stops and the user can see the requested page unhindered.

#### getCredentials()

The get credentials method handles organising the credentials that were passed in from the HTTP request. It will then pass them off to the [`getUser()`](#getuser) and `[checkCredentials()](#checkcredentials)` methods. This method also sets the `Securiy::LAST_USERNAME` session variable with the provided username.

#### getUser()

The first thing this method does is test the CSRF ([Cross-Site Request Forgery](https://en.wikipedia.org/wiki/Cross-site_request_forgery)) token that was provided with the form submission. If that fails the user is not authenticated and an error is thrown.

> Symfony is really good about using CSRF protection on forms. It's baked right in.

Otherwise it looks up the user by the provided username. If that fails another exception is thrown and the authentication process stops.

On success the appropriate `User` object is returned.

#### checkCredentials()

Check credentials does exactly what the name implies. It tests the provided password from the form against what is stored in the database, and returns `true` if it matches and `false` if it doesn't

#### onAuthenticationSucess()

If we get to this method everything worked and the user is authenticated. This method determines what page the, now authenticated, user should be redirected to.

It first checks to see if the user was redirected to the login form from another page. This would happen if they tried to visit their dashboard directly, or if their login session had timed out. If so, they are sent back there to continue what they were doing.

Otherwise, they went to the login page first, so we redirect them to the home page. (I set that up earlier.)

#### getLoginUrl()

This is a convenience function that returns the url for the `app_login` route.

### src/Controller/SecurityController.php

This class handles the routing for `/login` and `/logout`.

#### login()

By default this method simply retrieves any authentication errors and the last username that was used in authentication. These values are passed to the login template.

Basically this handles the first display of the form, and any subsequent displays when there are authentication errors or session timeouts.

There is a commented out section which can be implemented to redirect logged in users to a specific route so they don't need to login again.

#### logout()

This may be the most interesting method I've ever seen:

```php
/**
 * @Route("/logout", name="app_logout")
 */
public function logout()
{
    throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
}
```

Essentially it's a placeholder, just to register the `app_logout` route with the app.

### templates/security/login.html.twig

This one is worth a look:

```twig
{% extends 'base.html.twig' %}

{% block title %}Log in!{% endblock %}

{% block body %}
<form method="post">
    {% if error %}
        <div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}

    {% if app.user %}
        <div class="mb-3">
            You are logged in as {{ app.user.username }}, <a href="{{ path('app_logout') }}">Logout</a>
        </div>
    {% endif %}

    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="inputUsername" class="sr-only">Username</label>
    <input type="text" value="{{ last_username }}" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

    <input type="hidden" name="_csrf_token"
           value="{{ csrf_token('authenticate') }}"
    >

    {#
        Uncomment this section and add a remember_me option below your firewall to activate remember me functionality.
        See https://symfony.com/doc/current/security/remember_me.html

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="_remember_me"> Remember me
            </label>
        </div>
    #}

    <button class="btn btn-lg btn-primary" type="submit">
        Sign in
    </button>
</form>
{% endblock %}
```

The template first tests for the `error` variable which comes from failed authentication attempts.

Next it tests for the `app.user` object, which is an app global that is set when a user is autheticated. (We'll need this for the `post/add` page.) If present, it lets the user know that they are logged in, and provides a "Logout" option.

> Of course if we turn on the logged-in redirect in the SecurityCotroller this page will never show up, so this is an either/or situation.

Then it displays the login form. Note the hidden `_csrf_token` field. This is where the authentication token is generated by the system. ([More on this twig function.](https://symfony.com/doc/current/reference/twig_reference.html#csrf-token))

#### Template Modification

`make:auth` encouraged us to "Review & adapt the login template: templates/security/login.html.twig.". There are 2 obvious things to do.

First. Pretty it up. This is really plain. The bootstrap hooks are there, but the css and js is not implemented because I haven't done that site wide. (My intention is to use CSS-grid to build my layout.)

Second. See that commented out section. It shows how we can implement "Remember Me" functionality for the site. It's even thoughtful enough to provide a [link](https://symfony.com/doc/current/security/remember_me.html) to exlpain how to add that feature to the firewall (in `config/packages/security.yaml`).

I'm not going to do either of these things right now, because I just want to get the ability to enter a post.

### config/packages/security.yaml

This is the only file that was modified (instead of created) by the `make:auth` process.

Here's what was added to the `security:firewalls:main` section under the `anonymous: true` line.

```yaml
guard:
  authenticators:
    - App\Security\LoginFormAuthenticator
logout:
  path: app_logout
  # where to redirect after logout
  # target: app_any_route
```

This is how the app knows what class to use for authentication and also where it hijacks the logout process.

## Next Steps.

Now that we have an understanding of the authentication process, it's time to finish up the ability to post a new item to the site.

### [<< Step 8 - Making Posts](SymfonyBlogPart8.md) | [Step 10 - Submitting a Post >>](SymfonyBlogPart10.md)
