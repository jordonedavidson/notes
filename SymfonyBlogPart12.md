# Creating a Blog from Scratch Using Symfony 4

## Part 12: Post to Post Navigation

**Goals**

- Add navigation to the previous and next posts on a post page.

## Getting the Links

Blogs, by definition, are **time based** (where posts are organised by published date) and any blog worth its salt offers a way to move to the Previous and Next posts via links on the page.

To do this we need to:

* Retrieve the posts on either sied of the current post 
* Use that data to display the links on the post page.

## Step 1. Get the Previous and Next Posts

To get the information for the previous and next posts we need to consult the database.

Essentially we need to say: Give me the post immediately before and immediately after this one when the list of posts is sorted by date.

> We can't just ask for the posts with id +/-1 from the current post because we allow for the author to set the post date in the form.

Now, generally we want to make as few trips to the database as possible, and we've already made one call to get the post by the url slug [Step 6](SymfonyBlogPart6#the-route) so it would be nice if we could wrap this up in only one call to the database.

Unfortunately (a) that is considerably trickier than you might think in MySQL and (b) after doing some research it appears that even if we went to the trouble of getting both results in one call, that call takes way more time than making the individual calls for the previous and next posts.

### Adding Methods to the Repository

In Symfony/Doctrine all data calls are methods in the **<type>Repository** class (PostRepository in our case). The simple queries are all layed out in the annotation for the class, [Step 4: Using the Repository](SymfonyBlogPart4#using-the-repository) while more advanced queries are added as methods to the class.

We'll be adding 2 methods: `findPreviousPostByDate` and `findNextPostByDate`. each method will take the current post object as a parameter.

### Doctrine Query Builder

While we can use SQL directly in our app, we're using the Doctrine abstraction layer so we should try to avoid that if at all possible. The whole point of using an ORM is to easily be able to change the database used by the app without having to rewrite any code.

Doctrine provides a method for creating complex SQL queries via the [Doctrine Query Builder](https://symfony.com/doc/current/doctrine.html#querying-with-the-query-builder) in which we chain together a series of methods which add all the conditions you find in a statndard SQL query. 

The query in `findNextPostByDate` needs to return the post with the next newest `created_date` after the current post. This is how that looks

```php
/**
 * Get the next post after the provided post.
 *
 * @param Post $post The post to check.
 *
 * @return Post[] Returns an array of with the next post (or empty if there is none).
 */
public function findNextPostByDate(Post $post)
{
    return $this->createQueryBuilder('p')
        ->andWhere('p.id != :id')
        ->setParameter('id', $post->getId())
        ->andWhere('p.created_date >= :date')
        ->setParameter('date', $post->getCreatedDate())
        ->orderBy('p.created_date', 'ASC')
        ->setMaxResults(1)
        ->getQuery()
        ->getResult();
}
```

As you can see this is a pretty straightforward function, and due to the well-named method names it's pretty easy to follow what this query does.

`p` is the ailas for the `post` table (this could have been anything but p is a simple representation).

The `andWhere` methods add conditions to the query. The `:id` and `:date` items are placeholders. This is a common method used for properly sanitizing variables which are passed into an expression. the `sprint_f` php command does something very similar.

`setParameter` handles injecting the variables into the query placeholders. In this case the `id` and `created_date` of the `Post` object that was passed to the method.

`orderBy` sets the ORDER BY clause in the query, while `setMaxResults` is the LIMIT. Since we only want at most one result from the query we set this to 1.

`getQuery` executes the query and `getResult` returns the results of the query as an array of `Post` objects. 

Very slick.

`findPreviousPostByDate` is virtually identical except that we use <= for the date and order the results in descending order.

## Step 2. Update the Controller

Within the `PostController` we can now call the new repository methods to return the next and previous posts, (if they exist) and pass that data on to the template.

We'll use an array called `nav` to hold the data, and add member arrays for `next` and `prev` to pass the link data for the other posts. The update to the `getPostBySlug` method looks like this:

```php
/**
 * @var array The navigation array.
 */
$nav = [];

$prev = $postRepository->findPreviousPostByDate($post);
$next = $postRepository->findNextPostByDate($post);

if (count($prev) > 0) {
    $nav['prev'] = [
        'title' => $prev[0]->getTitle(),
        'slug' => $prev[0]->getSlug(),
    ];
}

if (count($next) > 0) {
    $nav['next'] = [
        'title' => $next[0]->getTitle(),
        'slug' => $next[0]->getSlug(),
    ];
}
```

Since we are making simple links we only need to pass the `slug` and `title` fields of the posts. Remember that the repository methods we created return an array, so the desired post is the first (only) member of that array.

This data will be available in the template as the `nav` object.

## Step 3. Update the Twig Template

I breifly considered creating a dedicated navigation template to hold this, however this is really the only place on the site it will be used so it made more sense to just put it in the `post.html.twig` template.

The nav will go at the bottom of the article. In it we need to test if there is a previous or next link to display. We may be at the first or last post after all. 

Twig provides a special test in the `if` statement for checking if a specific key in an array exists, `is defined` If the test passes we can draw the link on the page. We add this navigation section directly after the `article` tag inside the `body` block of the template.

```twig
<nav class="nav">
{% if nav.prev is defined %}
    <a href="{{ path('post', { 'slug':nav.prev.slug }) }}" title="{{nav.prev.title}}">&laquo; {{nav.prev.title}}</a>
{% endif %}
{% if nav.next is defined %}
    <a href="{{ path('post', { 'slug':nav.next.slug }) }}" title="{{nav.next.title}}">{{nav.next.title}} &raquo;</a>
{% endif %}
</nav>
```
And here's the result on a test post.

![Post With Navigation](images/PageWithNavigation.png)

### [<< Part 11 - Translating Markdown](SymfonyBlogPart11)