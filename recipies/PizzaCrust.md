# Awesome Cauliflower Pizza Crust

> Based on [Cauliflower Pizza Crust (Low Carb Keto Pizza)](https://downshiftology.com/wprm_print/38865) and __The Chef Show__ S1E5 *feat. Robert Rodriguez*

__Author__ Jordon Davidson, July 2020

__Prep Time__ 20 Minutes

__Cook Time__ Crust: 25 Minutes, Pizza 10 Minutes

## Ingredients

* 1.5 lbs Cauliflower Florets (one large head of cauliflower)
* 1/2 cup Shredded Cheese (Cheddar, Mozzerella, Monteray Jack, *etc.*)
* 1/4 cup Grated Parmesian
* 2 Large Eggs
* 1 teaspoon Italian Seasoning
* 1 teaspoon Red Chili Flakes (optional)
* Salt and Pepper to taste

## Instructions

Using a food processor or a grater grate the cauliflower florets into a microwave safe bowl.

Cover the bowl and microwave on high for 10 minutes.

Meanwhile pre-heat your oven to 400°F and get cover a large baking sheet or pizza stone with parchement paper.

Turn the cooked cauliflower (__Attention:__ it will be some hot!) out onto a tea towel to cool.

Once cool (about 10 min) gather up the edges of the tea towel, keeping all the cauliflower inside, and squeeze out all the water from the cauliflower. 

> This takes a lot of effort. You'd be suprised how much water is in there. When you think you're done, squeeze some more. Seriously, this is the key to the whole recipe.

In a mixing bowl stir together the Eggs, both Cheeses and the Spices. 

Add the drained cauliflower to the mixture and knead it in completely.

Dump the "dough" onto the parchment lined baking sheet and press the dough into a consitent layer approx 1/4" thick. Make the edge just a bit thicker, creating a rim around the pie.

Bake in the oven for 25 minutes.

While the crust is baking go ahead prep your pizza toppings.

Once the baking is complete, add your toppings and return to the oven for approx 10 minutes to finish the Pizza.

## Notes

Go easy on the pizza sauce with this crust. It is very absorbent and prone to falling apart.

For a cheesier crust double up the shredded cheese.

While it won't hold its shape like its bread-based cousin, thanks to the 2 eggs this crust is suprisingly sturdy. However, I still recommend eating the pizza with a knife and fork.