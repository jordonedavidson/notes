# Creating a Blog from Scratch Using Symfony 4

## Part 5: Blog Post Test Part 2

**Goals**

- Successful test for post listing.

## Planning for the Test

In the last post we tested that the posts page correctly did not show any posts when there were none. Now we need to do something a little harder: Test that posts do show up when they exist.

On the surface this seems pretty straight forward, just look at the page when the data is present, and make sure that it shows correctly on the page. However, as we know, testing requires us to be in complete control of the data. So the question becomes, "How do we get known data into the database to test?"

The answer is, of course, "We put it there."

At this point we haven't built any administration for the blog, so, for now, we'll bake the necessary tools into the test.

## Step 1: Adding Posts

A reminder: this is the test method we are working with:

```php
public function testShowsListOfPosts()
{
  $crawler = $this->client->request('GET', '/post');

  $this->assertResponseIsSuccessful();
  $this->assertSelectorExists('#notes_listing');
}
```

In order to test that the posts display properly we need to have some posts, or at least a post to start with.

Saving data to the db is pretty straightforward with Doctrine:

1. Instatntiate an entity
2. Add data to the entity
3. Persist (Prepare to save) the entity to the DB
4. Save the data to the db.

Adding this to the method should do the trick:

```php
/**
 * @var \App\Entity\Post $post A test post
 */
$post = new Post();

// Add data to the post.
$post
  ->setTitle('Test Post')
  ->setSlug('test-post')
  ->setPublished(true)
  ->setBody('# Test Post')
  ->setCreatedDate(new \DateTime('2019-01-01 09:00:00'))
  ->setAuthor('Bob Author');

// Persist and save the post.
$this->em->persist($post);
$this->em->flush();
```

Unfortunately there are 2 issues with this code.

First, it's not very re-usable, If we want to add two (or 10) posts to the db then we have to do a lot of copy/paste in the method.

Secondly, and maybe more importantly, the **author** member of the post needs to be an instance of the **User** entity class. A string just won't cut it.

Since we'll be needing to perform these actions a lot in this test class we'll add some private functions to make things easier.

### Getting an Author for the Post

Remember that the entire test db gets cleaned out after every test. So we want a function that will simultaneously create an author with data we control and return the object to us.

```php
/**
 * Create an Author
 *
 * @param string $username The username for the User.
 * @param string $realname The real name for the User.
 *
 * @return App\Entity\User The User object.
 */
private function getNewUser(string $username = 'user', string $realname = 'Test User') : User
{
    $user = new User();
    $user->setUsername($username);
    $user->setPassword('password');
    $user->setRealname($realname);

    $this->em->persist($user);
    $this->em->flush();

    return $user;
}
```

We can pass the author's username and real name to this function which will both save that user to the database and return the User object for use in our post.

### Saving Posts

The post saving method will emulate the code we used in the first try above. It will take the Post object and an associative array of the fields for the post. It will assign that data to the Post and save it to the database.

> This method won't be particularily robust, and it doesn't need to be since we are in control of all sides of it.

```php
/**
 * Save a post.
 *
 * @param \App\Entity\Post $post The post entity to save
 * @param array $fields The dataset for the post
 */
private function savePost(Post $post, array $fields): void
{
    // load minimal data for post
    $post
      ->setTitle($fields['title'])
      ->setSlug($fields['slug'])
      ->setPublished($fields['published'])
      ->setBody($fields['body'])
      ->setCreatedDate($fields['created_date'])
      ->setAuthor($fields['author']);

    // Save the post
    $this->em->persist($post);
    $this->em->flush();
}
```

Now we can call these methods in the test like this:

```php
public function testShowsListOfPosts()
{
  /**
   * @var \App\Entity\Post $post A test post
   */
  $post = new Post();

  /**
   * @var User Create and retrieve the post author.
   */
  $author = $this->getNewUser('author', 'Test Author');

  $this->savePost($post, [
      'title' => 'Test Post',
      'slug' => 'test-post',
      'published' => 'true',
      'body' => '# Test Post',
      'created_date' => new \DateTime('2019-01-01 09:00:00'),
      'author' => $author,
  ]);

  $crawler = $this->client->request('GET', '/post');

  $this->assertResponseIsSuccessful();
  $this->assertSelectorExists('#notes_listing');
}
```

And this test works. Obviously we can go into more detail in the testing. Like making sure the Title and link display correctly. We can even add multiple posts to ensure that they all show up. However, for now this is enough.

## Next Steps

Next time we will finish up with testing the Post controller by testing a single post page.

### [<< Step 4 - Blog Post Test Part 1](SymfonyBlogPart4.md) | [Step 6 - Blog Post Test Part 3 >>](SymfonyBlogPart6.md)
