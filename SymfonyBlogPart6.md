# Creating a Blog from Scratch Using Symfony 4

## Part 6: Blog Post Test Part 3

**Goals**

- Successful test for post page.

Next up the test for the post display. Back in [Part 4](./SymfonyBlogPart4#links) we discussed that the route for a single post would be `/post/{slug}` so lets get to testing that page.

## The Test

As always with TDD we start with the test. In the test we will:

- Create an author
- Create and save a post
- Read in the page
- Verify that the Post title is present in an `<h1>` tag

We've seen all the parts to this test in previous sessions so this will be pretty staightforward.

```php
/**
 * Make sure the post page shows up using the slug test-post
 */
public function testPostPageDisplaysCorrectly()
{
  /**
   * @var User Create and retrieve the post author.
   */
  $author = $this->getNewUser('author', 'Test Author');

  $this->savePost(new Post(), [
      'title' => 'Test Post',
      'slug' => 'test-post',
      'published' => 'true',
      'body' => '# Test Post',
      'created_date' => new \DateTime('2019-01-01 09:00:00'),
      'author' => $author,
  ]);

  $crawler = $this->client->request('GET', '/post/test-post');

  $this->assertResponseIsSuccessful();
  $this->assertSelectorTextContains('article h1', 'Test Post');
}
```

Naturally this fails, since neither the route nor the template are in place. What we know at this point is the Route will recieve the slug for lookup, and the template should display the Post's title in an `<h1>` tag inside the `<article>` block.

## The Route

A neat thing about routing annotations is that they can take variables. Otherwise we would need to create a specific route for every post on the site! Not very practical.

This route will take the `slug` variable which we show in the annotaion like this:

```php
/**
 * @Route("/post/{slug}", name="post", methods={"GET"})
 */
```

The neat thing about this is, whatever we put inside the {} in the route is the name of the parameter that will be passed to the method. So `{slug}` is `$slug`, while `{dude}` would be `$dude`

The method will also use Symfony's dependency injection to pick up the PostRepository, giving us access to the find method we need.

Finally we'll pass the post title, created date, and body to the twig template:

```php
/**
 * @Route("/post/{slug}", name="post", methods={"GET"})
 *
 * @param string $slug Provided by route
 * @param \App\Repository\PostRepository $postRepository
 *
 * @return Response
 */
public function getPostBySlug($slug, PostRepository $postRepository): Response
{
  /**
   * @var \App\Entity\Post The post returned by the slug
   */
  $post = $postRepository->findOneBy(['slug' => $slug]

  return $this->render(
      'post/post.html.twig',
      [
          'title' => $post->getTitle(),
          'date' => $post->getCreatedDate()->format('j M Y'),
          'body' => $post->getBody(),
      ]
  );
}
```

## The Template

From the route method we know that the template will a) be called `post.html.twig` and b) it will receive the variables `title`, `date`, and `body`

Let's put that together.

```twig
{# templates/post/post.html.twig #}
{% extends 'base.html.twig' %}

{% block title %}{{ title }} | Tech Notes{% endblock %}

{% block body %}
    <article>
        <h1>{{ title }}</h1>
        <p class="date">Published: {{ date }}</p>
        <div class="body">
            {{ body }}
        </div>
    </article>
{% endblock %}
```

## Run the Test

Now we hold our breath and run the test:

```console
> ./bin/phpunit
PHPUnit 7.5.14 by Sebastian Bergmann and contributors.

Testing Project Test Suite
....                                                                4 / 4 (100%)

Time: 675 ms, Memory: 22.00 MB

OK (4 tests, 11 assertions)
```

**Wassail!**

## Next Steps.

It's time to add some administration to the site so we can get the data into it for real.

We need some user management, including login and out, as well as an interface to add/edit and delete posts. And, finally we'll pretty things up by using Markdown for the post entries.

### [<< Step 5 - Blog Post Test Part 2](SymfonyBlogPart5.md) | [Step 7 - User Registsration >>](SymfonyBlogPart7.md)
