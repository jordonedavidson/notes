# Creating a Blog from Scratch Using Symfony 4

## Part 8: Making Posts

**Goals**

- Create a page where post content can be entered
- Update the output to translate Markdown

## Input Form

The first thing we need in order to get posts into the database is an input form. Symfony has lengthy [documentation](https://symfony.com/doc/current/forms.html) on this subject. And there is a quick way to get started using the console's [`make:form`](https://symfony.com/blog/new-and-improved-generators-for-makerbundle#improved-the-make-form-generator) command.

This tool asks for the name of the form type to create and the Doctrine entity to connect the form to. We'll use `PostFormType` and `Post`.

```console
> ./bin/console make:form PostFormType

 The name of Entity or fully qualified model class name that the new form will be bound to (empty for none):
 > Post

 created: src/Form/PostFormType.php

  Success!

 Next: Add fields to your form and start using it.
 Find the documentation at https://symfony.com/doc/current/forms.html
```

> By convention Symfony form classes end in "Type"

Here is what was created for us:

```php
# /src/Form/PostType.php
namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostFormType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('title')
      ->add('slug')
      ->add('abstract')
      ->add('body')
      ->add('created_date')
      ->add('published')
      ->add('author')
      ->add('tags')
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => Post::class,
    ]);
  }
}
```

Which looks a lot like the RegistrationType we created before. However, this is pretty bare bones, so there is still some work to do before it is useable.

## Displaying the Form.

> For my sanity while getting this working I've set the app to use Symfony's built in Bootstrap 4 integration for displaying the forms.
>
> ```twig
> # config/packages/twig.yaml
> twig:
>     form_themes: ['bootstrap_4_layout.html.twig']
> ```
>
> I'll tweak this later.

We need to create a route for adding new posts. `/post/add` should do the trick. I'm going to crib a lot of the handler code from the RegistrationController.

Here is the basics to get the ball rolling

```php
# /src/Controller/PostController
/**
 * @Route("post/add", name="add_post", methods={"GET", "POST"})
 *
 * @param \Symfony\Component\HttpFoundation\Request $request The parameters from the  POST.
 * @return \Symfony\Component\HttpFoundation\Response
 */
public function addNewPost(Request $request): Response
{

  $post = new Post();
  $form = $this->createForm(PostFormType::class, $post);
  $form->handleRequest($request);

  # The POST response
  /**
   * @todo Add saving of the post here
   */

  # The GET response
  return $this->render('post/add.html.twig', [
      'postForm' => $form->createView(),
  ]);
}
```

### The Template

In the controller we said that we would render the form in the `post/add.html.twig` template, so we need one of those.

```twig
{# /templates/post/add.html.twig #}
{% extends 'base.html.twig' %}

{% block title %}Add New Note{% endblock %}

{% block body %}
  <article class="container">
    <h1>Add a New Tech Note</h1>
    {{ form(postForm) }}
  <article>
{% endblock %}
```

Heading to that page gives a Big 'Ol Error™:

#### Call to a member function getTitle() on null

This points out an interesting "feature" of Symfony routes: they resolve in the order they appear in the controller.

In this case `/post/add` looks a lot like `post/{slug}` So the app tries to find a post with the slug "add" to display. Since it doesn't find one, it errors.

> ToDo: Add 404 routing to the `getPostBySlug()` method

No problem, just move the new route above that and we should be good to go.

> Yes it does. After removing the author field from the form. That will have to be handled inside the controller.

![New Post](./images/NewNoteForm.png)

Of course this form is missing something. A submit button. To make that happen we need to use the form functions in the same manner as the Registration Form.

Here is the updated twig block

```twig
{% block body %}
  <article class="container">
    <h1>Add a New Tech Note</h1>
    {{ form_start(postForm) }}
      {{ form_row(postForm.title) }}
      {{ form_row(postForm.slug) }}
      {{ form_row(postForm.abstract) }}
      {{ form_row(postForm.body) }}
      {{ form_row(postForm.published) }}
      {{ form_row(postForm.tags) }}
      {{ form_rest(postForm) }}
      <button type="submit" class="btn-success">Submit</button>
    {{ form_end(postForm) }}
  <article>
{% endblock %}
```

## Form Fields and Validation

Symphony forms does a pretty good job of deciding how to display a form based on the types of the fields in the database. VARCHAR are simple text inputs, TEXT become textareas, BOOLEAN becomes a checkbox, the DATETIME field becomes a series of selectors, and Colections become a multi selector box. (Since there are no tags currently this is empty.)

We have a lot of control over this, both from the form builder and from within twig. And we'll get to that later.

For now let's see what the submitted form sends using the tools in the Symfony profiler.

## Handling the Form Submission

Looking into the Request/Response section The post parameters show:

```
[▼
  "title" => "New Note"
  "slug" => "new-note"
  "abstract" => ""
  "body" => """
    # New Note\r\n
    \r\n
    This is a new note
    """
  "published" => "1"
  "created_date" => [▼
    "date" => [▼
      "year" => "2019"
      "month" => "9"
      "day" => "10"
    ]
    "time" => [▼
      "hour" => "11"
      "minute" => "0"
    ]
  ]
  "_token" => "Sp8gCWPfBFxTxCy8qFRx-FmwFVAzLZ3lEcqYCNHEZCc"
]
```

which is a nice array that we have to work with in posting the data to the db.

The basics of the wrtie to the db looks like:

```php
# The POST response
if ($form->isSubmitted() && $form->isValid()) {
    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($post);
    $entityManager->flush();
}
```

The `if` block conditions utilise two methods from the form handler class to verify that a) the form was sumbitted (no form submission, no work) and that it passed Symfony's internal verification process.

Then we use the Entity Manager to persist and save the post to the database.

Naturally, at this point the save won't work, as there is no User object to associate with the post. So we need to take a break to create the user login.

## Next Steps

Create the user login form and finish the post submission process.

> I realise I'm slacking on the TDD in this phase. It turns out this process is a bit convoluted, so any test I write would require all of this to be working first anyway. I'll pick those up after I get a working flow.

### [<< Step 7 - User Registsration](SymfonyBlogPart7.md) | [Step 9 - User Login >>](SymfonyBlogPart9.md)