# Elasticsearch install notes

## Caveats

Data: /usr/local/var/lib/elasticsearch/elasticsearch_jdavidson/

Logs: /usr/local/var/log/elasticsearch/elasticsearch_jdavidson.log

Plugins: /usr/local/var/elasticsearch/plugins/

Config: /usr/local/etc/elasticsearch/

To have launchd start elastic/tap/elasticsearch-full now and restart at login:

```console
brew services start elastic/tap/elasticsearch-full
```

Or, if you don't want/need a background service you can just run:

```console
elasticsearch
```
