# Creating a Blog from Scratch Using Symfony 4

## Part 11: Translating Markdown

**Goals**

- Translate the post body markdown.

## Converting Markdown Into HTML

This is suprisingly easy.

First we need to add the [KnpMarkdownBundle](https://github.com/KnpLabs/KnpMarkdownBundle) From KnpLabs to the project with:

```console
composer require knplabs/knp-markdown-bundle
```

This updates a few files and gives access to the `Knp\Bundle\MarkdownBundle\MarkdownParserInterface` in PHP (where we can do the translating directly,) and to the twig translator `|markdown`

So all we need to do to parse the markdown on the page is make one small change to the twig template `templates/post/post.html.twig`:

```twig
{{ body|markdown }}
```

**That's it!**

## Next Steps.

As I mentioned in Part 10 there are a number of things left to do before this blog is *ready for prime time*. The interesting things I'll record, and the boring things (that are just more of the same things already covered) I'll skip. 

Thanks for coming along for the ride.

### [<< Part 10 - Submitting a Post](SymfonyBlogPart10) | [Part 12 - Post to Post Navigation](SymfonyBlogPart12)