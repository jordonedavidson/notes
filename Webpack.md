# Webpack HowTo

## Prereqs

- Node.js
- npm

## Installation

```bash
npm install webpack webpack-cli --save-dev
```

Installs both webpack and the command line interface as development requirements

## Defaults

By default webpack input file is `./src/index.js` and output is `./dist/main.js`

## Running from Command Line: [API Docs](https://webpack.js.org/api/cli/)

**Basic:**

```bash
./node_modules/.bin/webpack <entry> [<entry>] -o <output>
```

Where the entry points are a list of files and the output file is specified with the -o flag.

**_Note:_** Webpack will complain about the 'mode' option not being set. To avoid this add `--mode="development"` or `--mode="production"` to the command.

## Running from a Config File **webpack.config.js**

**_Note_** This file must use the `module.exports = { }` syntax because we don't have access to ES6

Using a config file is much more extensible than using the command line especially for more complex functionality. This way we can simply run:

```bash
./node_modules/.bin/webpack
```

Remember to add the `mode: [development|production]` parameter to the object to avoid the warning.

_Setting Custom Entry and Output_
The `entry` parameter needs to be a full path from root to the input file. ex: `./src/main.js`

The `output` parameter is an object that requires `path` and `filename` parameters. `path` must be an **absolute path** so we use the `path.resolve` function as follows (to target the build directory).

```javascript
// Include path.
const path = require("path");

module.exports = {
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "output_filename.js"
  }
};
```

We need to import the `path` class to get access to its `resolve` method. `path.resolve` takes 2 parameters, `__dirname` which is a magic function that resolves to the absolute path to the current directory, and the path to the directory in the local file structure (`build` in this case.)

**Watch Flag**

Running webpack with the `-w` flag puts it in watch mode, so that any updates to the input files results in an automatic build. This takes over the terminal window, use `CTRL-c` to stop the process.

## Transpiling

In order to use JSX (React) and ES6 (or higher) in our code we need to convert that to ES5 based JS so it can run in the browser. The most common tool for this transformation is [Babel](https://babeljs.io/).

Webpack accesses extra functionality (like transpiling) by using [**loaders**](https://webpack.js.org/concepts/loaders/#root). In this case the `babel-loader`.

### babel-loader

To include babel in the project run:

```bash
npm install --save-dev babel-loader @babel/core
```

**Using the loader in webpack.config.js**

Include the babel-loader (and other loaders) in the `module.rules` array.

Each element in this array is an object that includes the following parameters:

- test : A regex to describe what files the loader should work on
- exclude : A regex to describe what group of files the loader should not look at (optional)
- use: A string of the loader to use, or an array of objects which describe which loader to use and an optional `options` array that configures the loader.

When `use` is an array the loaders are processed from bottom to top (_i.e._ the last loader listed is executed, then the result of that is passed to the next loader above, etc.)

Babel requires a config file called `.babelrc` in order to do anything for us. In this file we can enable various plugins to use for transpiling.

This file contains a JSON object which minimally contains a `presets` array, listing the various items that we want to use babel on. The current [Babel setup docs](https://babeljs.io/setup#installation) reccomends the following basic setup to include transforms for ES2015+

```bash
npm install @babel/preset-env --save-dev
```

```JSON
{
  "presets": ["@babel/preset-env"]
}
```

It is also possible to include this object as the `babel` parameter inside `package.json`

## Webpack and CSS

Webpack is also useful for compiling `Sass` and `LESS` into vanilla css.

Including css via webpack has the advantage of only bundling the styles used by the app and styles can be imported only into the files that require them.

**Loaders**

For basic css handling webpack uses the `css-loader` and the `style-loader` in that order.

Add these to the the project with

```bash
npm install style-loader css-loader --save-dev
```

and set them up in webpack with the following rule:

```javascript
module: {
  rules: [
    {
      test: /\.css$/,
      use: [{ loader: "style-loader" }, { loader: "css-loader" }]
    }
  ];
}
```

css files are included in your app with a simple `import` call in the .js file. _ex._

```javascript
import "./styles.css";
```

**Sass**

Webpack is especially useful for processing Sass (`.scss') files into browser usable css. To use Sass in your project you will need to include the sass-loader for webpack and the node-sass module as dev requiements in your project.

```bash
npm install sass-loader node-sass --save-dev
```

The Webpack config handles sass pre processing with the following rule:

```javascript
module: {
  rules: [
    {
      test: /\.scss$/,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" },
        { loader: "sass-loader" }
      ]
    }
  ];
}
```

## Webpack and Images

Inline image handling is accomplished with the **url-loader** and **file-loader**. Include with:

```bash
npm install url-loader file-loader --save-dev
```

In Webpack add the image loader

```javascript
module: {
  rules: [
    test: /\.(png|jpg)$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          limit: 2000000
        }
      }
    ]
  ]
}
```

Limit is the max-file-size in bytes, under which the image will be rendered inline as a 64bit endoced data source. Larger images use the file-loader as a fallback. This can be an issue for urls in css files as the path to the resulting image is in the output directory, which is not properly reflected in the css.

## Code Splitting

Allows us to break code up into different bundles. There are 2 use cases for this.

1. Multiple entry points. Allows custom js for differing pages in an app to keep things lean and avoid loading unnceccessary js into each one.
2. Combining common js from multiple files into one js file that can be loaded and cached for the whole app.

### Multiple Entry Points

The entry parameter in the `webpack.config.js` file can also take an object of named entry points. For example:

```javascript
entry: {
  about: './dist/about',
  contact: './dist/contact'
}
```

To automatically handle the output of the multiple files we use a type of variable which lets us effectively loop over the names of the entry parameters. Like so:

```javascript
output: {
  path: path.resolve(__dirname, 'build'),
  filename: '[name].bundle.js'
}
```

This results in the files `about.bundle.js` and `contact.bundle.js` in the build directory. These script files can be included only in the pages they are need in.

### Handling Common JS in Multiple Entry Points.

**This section is not accurate. So far I have not been able to make it work**

Often when using multiple entry points there are common js modules that are imported, (React for instance) Using the SplitChunksPlugin (included by default) allows us to chunk out that common code for inclusion in all files loading once in the browser.

Webpack handles this in the `optimization` parameter.

**_Note_** This has a pretty complicated set of options that are outlined [here](https://webpack.js.org/plugins/split-chunks-plugin/#root) TL;DR use the defaults.

To engage this plugin use:

```javascript
optimization: {
  splitChunks: {
    chunks: "all"; // [all|async|initial]
  }
}
```

The resulting vendor bundle is included asynchronosly in each output file so the HTML files do not need to be updated.

**The above section needs updating**

## Webpack Dev Server

The webpack dev server is useful during development. While it is running it serves your app locally, and automatically re-runs webpack whenever the source files are updated. Basically it watches the files and automatically reloads the browser for you. Handy.

Install the server using

```bash
npm install webpack-dev-server --save-dev
```

In the config file tell the dev server where to look for the files to serve and what port you want to serve on.

```javascript
devServer: {
  contentBase: path.resolve(__dirname, './dist'),
  port: 5050
}
```

Finally add a script to the `package.json` file to make running it easy.

```JSON
"scripts" : {
  "start": "webpack-dev-server --open"
}
```

`npm start` should get everything going.
