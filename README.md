# Notes

A collection of markdown files with the notes I've taken on how to do things.

# Front-end Development

- [Webpack](./Webpack.md)

# Drupal

- [Drush Generate Command](./DrushGenerate.md)
- [Embed React in a Drupal Module (4 Parts)](./DrupalReactModulePart1.md)

# WordPress

- [WordPress CLI: Overview](./WPCliOverview.md)
- [WordPress CLI: Loader](./WPCliLoader.md)

# Symfony

- [Authentication in Doctrine](./DoctrineAuthentication.md)
- [The Symfony Console](./SymfonyConsole.md)
- [Creating a Blog From Scratch (12 Parts [Ongoing])](./SymfonyBlogIntro.md)

# Docker

- [Learning Docker](./LearningDocker.md)