# Creating a Blog from Scratch Using Symfony 4

## Part 3: The First Route (Finally)

**Goals**

- Use TDD to create our first tests
- Create a Controller to show the homepage of the website
- Learn about Routing in Symfony
- Maybe play with a Twig template.

## Viewing the Site.

Now that the database is ready to go it's time to start creating the site.

To see what the site looks like "as is", run the command

```console
symfony server:start
```

This spins up a webserver at http://localhost:8000. Go there in your web browser to see what we have.

![Symfony Default Page](./images/symfony_default_page.png)

Groovy, Apparently we have a fully working web server, complete with a friendly 404 page.

## Testing

Symfony uses the popular [PHPUnit](https://phpunit.de/) Testing suite for both unit (specific function/method) and functional (multiple functions together) testing.

PHPunit is run from the command line as:

```console
./bin/phpunit
```

The first time you run the command it will fill your terminal with a lot of installation information as it sets up the software for you.

Once that is complete, run the command again and we get

```console
PHPUnit 7.5.14 by Sebastian Bergmann and contributors.



Time: 50 ms, Memory: 4.00 MB

No tests executed!
```

So it works, and it seems there are no tests to run. Which makes sense because we haven't written any yet.

> This is a little different than working with other boilerplate project generators like `create-react-app` and `WP-CLI` which both include a trivial "It Works" test.

### The First Test

The concept of Test-Driven-Development is to write the tests first. Always!

I'm committed to giving that philosophy a whirl in the project so I'm going to start with a functional test to check out the home page.

The [documentation](https://symfony.com/doc/current/testing#functional-tests) on functional tests says that we need the `symfony/browser-kit` and `symfony/css-selector` packages as dev requirements. Fortunately for us these are already included in the default website-skeleton install in the `symphony/test-pack` bundle. 👍 For solid defaults.

To create the test file run

```console
./bin/console make:functional-test
```

We're prompted for the name of the functional test which is the name of the controller class with "Test" appended to it. It suggests "DefaultControllerTest" but I'm going to use HomePageController for the home page so I'll enter HomePageControllerTest at the prompt. And we have successfully created our first test in `/tests/HomePageControllerTest.php`.

```php
namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomePageControllerTest extends WebTestCase
{
    public function testSomething()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Hello World');
    }
}
```

Running the phpunit again gives us a big hairy fail. Exactly like it should. The mantra of TDD is

1. Write failing test
2. Write code to make test pass
3. Repeat

So far, so good. Now to create the home page controller and get a passing test.

## The Home Page

Step 1 is to make the HomePageController with the `console make:controller` command like so

```console
>./bin/console make:controller HomePageController

  created: src/Controller/HomePageController.php
  created: templates/home_page/index.html.twig


  Success!


 Next: Open your new controller class and add some pages!
```

The starter controller class has one method, index which displays the home page.

```php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/home/page", name="home_page")
     */
    public function index()
    {
        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HomePageController',
        ]);
    }
}
```

There are 2 important things to see here. The first is the **@Route** annotation. This is how Symfony handles page routing. Like many App based frameworks there is a single point of entry (`/public/index.php` in this case) All of the App content delivered from what looks like separate pages actually comes through that one file. The "Router" uses these annotations to know what data to send to the browser.

According the the route, we can see this page at `/home/page` and...
![Default Symfony home page](./images/home_page_route.png)

Awesome!

That brings up the second important thing in the index() method, the response it returns.

Index returns the result of a call to the class' render() function which takes the location of a template file and an array of values that are used in the template. This template takes the controller_name variable which is used in the heading on the page.

We'll delve into twig templates later. Now we need to keep going with our test.

We have a controller and a page, but the test still fails. Why?

Because the route for the HomePageController is /home/page and the test is looking for a page at / which is still the default Symfony debug 404 page.

All we need to do is change the @Route annotation to this

```php
/**
 * @Route("/", name="home_page")
 */
```

and try again.

HMM Still fails but with a different message.

```console
PHPUnit 7.5.14 by Sebastian Bergmann and contributors.

Testing Project Test Suite
F                                                                   1 / 1 (100%)

Time: 936 ms, Memory: 38.50 MB

There was 1 failure:

1) App\Tests\HomePageControllerTest::testSomething
Failed asserting that Symfony\Component\DomCrawler\Crawler Object &0000000025a7ef6b000000000e7dc4a0 (
   ...
) matches selector "h1" and has a node matching selector "h1" with content containing "Hello World".
```

Looking at the source of the home page we see that there is an H1 tag, but it doesn't have Hello World in it.

The simplest fix to that is to change the controller_name variable in the render method in the controller to simply say "World".

```
PHPUnit 7.5.14 by Sebastian Bergmann and contributors.

Testing Project Test Suite
.                                                                   1 / 1 (100%)

Time: 246 ms, Memory: 18.00 MB

OK (1 test, 3 assertions)
```

And, Success!

## Conclusion

This was obviously trivial, and it illustrates exactly how TDD works. Write a failing test, write enough code to make it pass and move on.

Functional tests are there to test how the app functions, how it looks and how it responds to user actions. A test like this one might be used to ensure that a copyright notice is visible, something that is very important to all websites.

## Next Steps

We didn't make it into the twig template this time, but we saw that the `make:controller` command creates one for each controller it makes, so that's something.

Next we'll continue on making controllers and routes as we create the controller for the individual blog posts.

## [<< Part 2 - More Tables](SymfonyBlogPart2.md) | [Part 4 - The Blog Post Controller >>](SymfonyBlogPart4.md)
