# WP-CLI Overview

Utilising the WordPerfect CLI (similar to the Symfony and Drupal cli tools) gives the WP admin and developer access to several tools that make life easier.

The standard install of WP-CLI gives us the `wp` command that we can use to manage WordPress and to create boilerplate for  post types, taxonomies, plugins, child themes, etc.

## Commands

Option | Description
--- | ---
plugin              | Manages plugins, including installs, activations, and updates.
  post              |   Manages posts, content, and meta.
  post-type         |   Retrieves details on the site's registered post types.
  rewrite           |   Lists or flushes the site's rewrite rules, updates the permalink structure.
  role              |   Manages user roles, including creating new roles and resetting to defaults.
  scaffold          |   Generates code for post types, taxonomies, plugins, child themes, etc.
  search-replace    |   Searches/replaces strings in the database.
  server            |   Launches PHP's built-in web server for a specific WordPress installation.
  shell             |   Opens an interactive PHP console for running and testing PHP code.
  sidebar           |   Lists registered sidebars.
  site              |   Creates, deletes, empties, moderates, and lists one or  more sites on a multisite installation.
  super-admin       |   Lists, adds, or removes super admin users on a multisite installation.
  taxonomy          |   Retrieves information about registered taxonomies.
  term              |   Manages taxonomy terms and term meta, with create, delete, and list commands.
  theme             |   Manages themes, including installs, activations, and updates.
  transient         |   Adds, gets, and deletes entries in the WordPress Transient Cache.
  user              |   Manages users, along with their roles, capabilities, and meta.
  wc widget            |   Manages widgets, including adding and moving them within sidebars.

### GLOBAL PARAMETERS

Parameter | Description
--- | ---
`--path=<path>` | Path to the WordPress files.
`--url=<url>` | Pretend request came from given URL. In multisite, this argument is how the target site is specified.
`--ssh=[<scheme>:][<user>@]<host|container>[:<port>][<path>]` | Perform operation against a remote server over SSH (or a container using scheme of "docker", "docker-compose", "vagrant").
`--http=<http>` | Perform operation against a remote WordPress installation over HTTP.
-`-user=<id|login|email>` | Set the WordPress user.
`--skip-plugins[=<plugins>]` |Skip loading all plugins, or a comma-separated list of plugins. _Note:_ mu-plugins are still loaded.
`-skip-themes[=<themes>]` | Skip loading all themes, or a comma-separated list of themes.
`--skip-packages` | Skip loading all installed packages.
`--require=<path>` | Load PHP file before running the command (may be used more than once).
`--[no-]color` | Whether to colorize the output.
`-debug[=<group>]` | Show all PHP errors and add verbosity to WP-CLI output. Built-in groups include: bootstrap, commandfactory, and help.
`--prompt[=<assoc>]` |Prompt the user to enter values for all command arguments, or a subset specified as comma-separated values.
`--quiet` | Suppress informational messages.

## Scaffolding code

We can quickly set up the base for several types of code with the `wp scaffold` command.

### DESCRIPTION

Generates code for post types, taxonomies, plugins, child themes, etc.

### SYNOPSIS

`wp scaffold <command>`

### SUBCOMMANDS

Option | Effect
--- | ---
  block            | Generates PHP, JS and CSS code for registering a Gutenberg block for a plugin or theme.
  child-theme      | Generates child theme based on an existing theme.
  plugin           | Generates starter code for a plugin.
  plugin-tests     | Generates files needed for running PHPUnit tests in a plugin.
  post-type        | Generates PHP code for registering a custom post type.
  taxonomy         | Generates PHP code for registering a custom taxonomy.
  theme-tests      | Generates files needed for running PHPUnit tests in a theme.
  underscores      | Generates starter code for a theme based on _s.

### EXAMPLES

```console
    # Generate a new plugin with unit tests
    $ wp scaffold plugin sample-plugin
    Success: Created plugin files.
    Success: Created test files.

    # Generate theme based on _s
    $ wp scaffold _s sample-theme --theme_name="Sample Theme" --author="John
    Doe"
    Success: Created theme 'Sample Theme'.

    # Generate code for post type registration in given theme
    $ wp scaffold post-type movie --label=Movie --theme=simple-life
    Success: Created
    /var/www/example.com/public_html/wp-content/themes/simple-life/post-types/movie.php
```

## Scaffolding a Plugin

### SYNOPSIS

```console
  wp scaffold plugin <slug> [--dir=<dirname>] [--plugin_name=<title>] [--plugin_description=<description>]
  [--plugin_author=<author>] [--plugin_author_uri=<url>] [--plugin_uri=<url>] [--skip-tests] [--ci=<provider>]
  [--activate] [--activate-network] [--force]
```

  The following files are always generated:

  * `plugin-slug.php` is the main PHP plugin file.
  * `readme.txt` is the readme file for the plugin.
  * `package.json` needed by NPM holds various metadata relevant to the project. Packages: `grunt`,
  `grunt-wp-i18n` and `grunt-wp-readme-to-markdown`.
  * `Gruntfile.js` is the JS file containing Grunt tasks. Tasks: `i18n` containing `addtextdomain` and
  `makepot`, `readme` containing `wp_readme_to_markdown`.
  * `.editorconfig` is the configuration file for Editor.
  * `.gitignore` tells which files (or patterns) git should ignore.
  * `.distignore` tells which files and folders should be ignored in distribution.

  The following files are also included unless the `--skip-tests` is used:

  * `phpunit.xml.dist` is the configuration file for PHPUnit.
  * `.travis.yml` is the configuration file for Travis CI. Use `--ci=<provider>` to select a different
  service.
  * `bin/install-wp-tests.sh` configures the WordPress test suite and a test database.
  * `tests/bootstrap.php` is the file that makes the current plugin active when running the test suite.
  * `tests/test-sample.php` is a sample file containing test cases.
  * `.phpcs.xml.dist` is a collection of PHP_CodeSniffer rules.

### OPTIONS

  `<slug>`  
    The internal name of the plugin.

  `[--dir=<dirname>]`  
    Put the new plugin in some arbitrary directory path. Plugin directory will be path plus supplied slug.

