# Creating a Blog from Scratch Using Symfony 4

## Or: Forcing Myself to _Really_ Understand Several Web Technologies All at the Same Time

This is the story of how this site came to be. (You can read my motivations [here](./BlogIntro.md))

I'm starting with the base install of the [symfony/website-skeleton](https://packagist.org/packages/symfony/website-skeleton) and working my way up to the completed project.

## Requirements

All sucessful projects start with a clearly defined set of requirements, here are mine.

- A functioning blog site
- Posts written in Markdown
- Tagging functionality with tag collection pages
- Responsive site using [CSS Grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout) (There's always time to learn one more thing!)
- Use of TDD (Test Driven Development) in the site build.

That last requirement is more of a goal really. I've tried to integrate testing into several projects since it was first introduced to me by my friend Fareed Quraishi ([@IamFareedQ](https://twitter.com/IamFareedQ)). However, I've never _really_ succeeded in geting proper unit and functional testing working in a website project. Hopefully it works out.

## The Plan

### Data Storage

I had toyed with the idea of using Mongo as the storage system, and even just using plain markdown files in a folder as the data source, but I've decided to use a traditional MySQL realtional for storing the post data.

I'm going to use [Doctrine Migrations](https://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html) right from the start to manage the structure of the DB (a first for me.)

### Symfony Console

Recently when building both Drupal and Wordpress Modules/Plugins I turned to command-line boilerplate file generators Drush and WP-CLI. Symfony has its own CLI called **console** and with it comes the `console make` command which does the same thing. I've toyed with this in various tutorials and I really think this is the way to go on this project.

> To install this feature follow the directions at [the Symfony site](https://symfony.com/download)

### TDD

As I mentioned above I'm really going to give this Test-Driven-Development thing a go. It feels foreign, clunky and slow, yet the industry swears by it, so I better learn how to do it right.

### Twig and CSS Grid

Symfony uses the [Twig templating system](https://twig.symfony.com/) for web display (Views I guess?) I've played with Mustache in the past and this looks similar, but with a bit more availability of programming logic in the template. I guess that's not as "pure" however it seems a bit easier to use for someone with an old-timer like me.

This is where I can flex my Design Muscles®™ by integrating CSS Grid into the look and feel. (Don't expect much, I'm a coder not an artist.) Responsive site FTW!

## Next Steps

Next I'll be designing the basic database entities, and creating the first route for the home page.

### [The Basics: DB and Routes](SymfonyBlogPart1.md)
