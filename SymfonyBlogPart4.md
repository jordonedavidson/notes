# Creating a Blog from Scratch Using Symfony 4

## Part 4: The Blog Post Controller

**Goals**

- Continue to use TDD
- Create a Controller to show summary of blog posts
- Use the same controller to show individual posts.
- Learn about Repositories
- Modify the Twig template for posts.

## Planning the Post Controller

The Post controller will be handling the meat of the website. It will show the list of posts, both in general and by tag, and show each individual post. If I get around to adding some kind of internal search functionality, it will probably handle that too.

Initially I want to start simply. A page to list the latest posts and a page to display a given post. This means there will be at least 2 routes handled in the controller, and 2 twig templates for the output. And, of course, one test class to make sure it all works as designed.

## Step 1: Write the Tests

**_Note_** In order to make functional tests run properly in a data-driven environment like a blog, it is important to safely control the contents of the database during testing. This section assumes these controls are in place. See the post on [Functional Testing](./Testing.md) for details.

As before I use `console make:functional-test` to create the boilerplate for `PostCotrollerTest.php`. The first thing to do is to add the setUp() and tearDown() functionality (per the Functional Testing post) we need.

The first test is simple, we want to make sure that when there are no posts to show that we get a note to that effect.

> Remember: Test method names are _very_ descriptive.

```php
public function testDisplaysNoPostsMessageWhenThereAreNoPosts()
{
    $crawler = $this->client->request('GET', '/post');

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('.test_no_notes', 'There are no notes');
}
```

This test assumes a few things.

- The route to the main post page is /post
- The no notes message will be inside a container with the class `test_no_notes`

> Using special classes with the `test_` prefix is a fairly common technique to allow the template and its CSS to change over time without having to update the test files.

Oh, the member variable `client` is instantiated in the setUp() method using the `static::createClient()` method

For completeness:

```php
/**
 * @var $client
 */
private $client;

/**
 * {@inheritdoc}
 */
protected function setUp(): void
{
  # Database cleanup methods.

  // Instantiate the web client.
  $this->client = static::createClient();
}
```

Naturally, running this test fails since there is no Post controller, so no route or template file exists.

Use `console make:controller PostController` to get the files

- `src/Entity/PostController.php`
- `src/Repository/PostRepository.php`
- `templates/post/index.html.twig`

The test still fails (of course), but the route `/post` exists. It would be easy to make the test pass by simply adding the desired text in the desired class, but that really doesn't help much. Knowing what we are trying to accomplish will direct our efforts.

Let's modify the twig template to handle both cases, posts and no posts.

First we add a test for when there are posts. In it we'll assume that posts are displayed in a container with the id `notes_listing`.

```php
public function testShowsListOfPosts()
{
  $crawler = $this->client->request('GET', '/post');

  $this->assertResponseIsSuccessful();
  $this->assertSelectorExists('#notes_listing');
}
```

_Note_: There is a lot that will need to be added to this test later.

## Step 2 Update the Twig Template.

Twig templates are how Symfony displays data to the client (web browser, api consumer, _etc._). [There is a lot to read about them (Version 2)](https://twig.symfony.com/doc/2.x/) but here we'll look at the very basics:

> Twig is currently on [Version 3](https://twig.symfony.com/doc/3.x/).

- Named blocks
- Template Extention
- Variables
- Conditionals
- Loops
- Links

> Hmm, that's actually a lot. fortunately it's very simple.

Here is the simple template `templates/post/index.html.twig`

```twig
{% extends 'base.html.twig' %}

{% block title %}Recent Notes | Tech Notes{% endblock %}

{% block body %}
  <article>

    <h1>Recent Notes</h1>

    {% if posts|length > 0 %}
    <ul id="notes_listing">
      {% for post in posts %}
      <li>
          <a href="{{ path('post', { 'slug':post.slug }) }}" title="{{ post.title|e }}">
            {{ post.title }}
          </a>
      </li>
      {% endfor %}
    </ul>
    {% endif %}

    {% if posts|length < 1 %}
    <h2 class="test_no_notes">There are no notes</h2>
    {% endif %}

  <article>
{% endblock %}
```

### Named Blocks

Twig uses braces for all of it's tags, anything enclosed in `{ }` is a substitution or a function of some kind.

`{% block <blockname> %} ... {% endblock %}` containers name the HTML that they surround. Why do that? Well...

### Extending Other Templates

It wouldn't make sense to duplicate the common parts of a webpage on each template, so twig templates can extend one another, just like PHP classes.

For the whole app there is a main template called `base.html.twig` which contains the basic structure of most of the web pages on the site. In it are more named blocks, which are replaced by blocks of the same name in any template that extends it.

To extend another template use the `{% extends 'template_name.html.twig' %}` tag on the first line of the extention template.

Our template extends `base.html.twig` which is currently

```twig
<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8">
      <title>{% block title %}Welcome!{% endblock %}</title>
      <link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png" />
      {% block stylesheets %}{% endblock %}
  </head>
  <body>
      <header>
          <nav id="header_nav">
              <ul class="nav">
                  <li><a href="{{ path('home_page') }}" title="Home">Home</a></li>
                  <li><a href="#" title="About the Author">About</a></li>
                  <li><a href="{{ path('post_list') }}" title="Posts">Posts</a></li>
              </ul>
          </nav>
          {% block hero %}{% endblock %}
      </header>
      {% block body %}{% endblock %}
      {% block javascripts %}{% endblock %}
  </body>
</html>
```

The title and body blocks appear in both the base and the extention. The content of the blocks in the `index.twig.html` file will appear in the appropriate locations in the `base.html.twig` file.

### Variables

Back in the Home Page controller we saw the variable `controller_name` passed from the **index()** method to the `home_page/index.html.twig` file. Twig templates, like most templates, are expecting variables. They are called inside double-braces: `{{ variable_name }}`

If the variable is an object then its members are called using `.` notation:  
`{{ object.member }}`

The posts template uses the array `posts` (an array of post objects) in both [conditionals](#conditionals) and [loops](#loops).

### Conditionals

Twig templates can use simple conditionals like `if`, (but not `else`). We see these in the `if/endif` blocks of the template.

```twig
{% if posts|length > 0 %}
 ...
{% endif %}
```

This tests to see if the `posts` variable has any elements in it.

We see that because there is no `{% elseif %}` structure we need to test for the opposite condition with its own `if` statement. This is pretty common amongst templating languages.

### Loops

Twig provides various looping functions. Our template uses the `{% for %} ... {% endfor %}` structure to roll through the `posts` array. Notice how the post object's members are called in the list items.

### Links

Although it is possible to use hard-coded links in twig templates it's not a good idea. Why be locked down to a specific, poorly-thought-out route name? Instead we use the built-in `path()` function.

`path()` is responsible for translating the path to an internal route name into a url that can be used by the browser.

To see what paths are avaialble use the `console debug:router` command in the terminal. The value in the Name column of the resulting table is the internal path name for the route, (these are set up in the controller files.)

The nav bar in the base template uses `{{ path('post_list') }}` in the href to get to the posts page (I'd better remember that later!). In the posts listing template we see the more complex path `{{ path('post', { 'slug':post.slug }) }}` which tells twig to add the slug parameter to the post, separated by a '/'. This link will be handled by the controller method that uses the named route `post` with a path of `/post/{slug}` (More on that coming up.)

With what we have so far the test `testDisplaysNoPostsMessageWhenThereAreNoPosts()` should function with a fairly minimal controller.

## Step 3 Update the Controller.

The make controller process gave us a very familiar `PostController.php` file. (That's the way boilerplate works, it all looks the same.) Let's modify the index method to send some posts to the listing template.

### The Basics

First I'll connect the route `/post` to the index function using the `@Route` annotation. I'm also naming it `post_list` for use with the twig `path()` function and I'm restricting it to only respond to `GET` requests with the `methods` array.

Then I'll send a posts array to the template file with the `render()` function.

```php
 /** @Route("/post", name="post_list", methods={"GET"})
  *
  * @return Response
  */
public function index(): Response
{
  $posts = [];

  return $this->render(
    'post/index.html.twig',
    [
        'posts' => $posts,
    ]
  );
}
```

This will do the trick to make the first test pass, as we are sending back an empty array. Obviously I need to do more to actually get the posts. For this we use the associated Repository file.

### Using the Repository

The file `src/Repository/PostRepository.php` was created when I made the Post entity. In Symfony, repositories handle the transactions with the database.

Looking at the file there is a lot of commented out code. This is there to serve as a model for custom queries that you'll make going forward. Here's the basic file.

```php
namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Post::class);
  }
}
```

Not much to it. The most important parts of the file are the `@method` annotations. These describe the function signatures we use to get posts from the database. It also shows what we expect in return.

The `findAll()` method is the simplest to understand. It returns an array of Post entities (`Post[]`) when we call it. That array may be empty. We can use the `find()` method with an \$id parameter to get a single Post entity, or `null` if it doesn't exist.

These annotations are methods that exist on the PostRepository class, the same way they would if they were defined in the class body.

### Getting the Posts

In order to use the repository we'll use it in the PostConroller class like this.

```php

use App\Repository\PostRepository;
// Other use statements

class PostController extends AbstractController
{
  /**
   * @Route("/post", name="post_list", methods={"GET"})
   *
   * @param \App\Repository\PostRepository $postRepository
   * @return Response
   */
  public function index(PostRepository $postRepository): Response
  {

    /**
      * Get the posts
      *
      * @var array
      */
    $posts = $postRepository->findAll();

    return $this->render(
        'post/index.html.twig',
        [
            'posts' => $posts,
        ]
    );
  }
}
```

I've injected the PostRepository into the method, then used its findAll() method to return an array of the posts. This array is passed as the `posts` variable to the template.

This is enough to make the empty test pass for real.

## Next Steps

Next time we'll see what we need to do to make the test for existing posts pass.

### [<< Step 3 - The First Route](SymfonyBlogPart3.md) | [Step 5 - Blog Post Test Part 2 >>](SymfonyBlogPart5.md)