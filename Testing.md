## Functional Testing in Symfony App

## Handling DB Interaction

Testing the app necessarily means database interactions are going to happen. Naturrally, for testing we need consistent database content and we don't want to mess up the _real_ site content.

To take care of this we need a dedicated testing database that mirrors the structure of the live one, but without the data.

Symfony gives us the ability to do this programatically with dotenv files and Doctrine.

### .env.test

The testing dotenv file contains overrides for variables used only in the `test` environemnt. The `symphony/website-skeleton` provides the `.env.test` file with some basic settings in it.

To this we add the line

```php
# testing database
DATABASE_URL=mysql://db_user:db_pass@127.0.0.1:3306/test_database
```

which is the connection string for the test db. Where the user, password and database name are specified to your environment.

> Practically this will be identical to the same line in `.env` with "\_test" applied to the end of the db name.

### Create the Test Database

From the command line console tool crateing the database is the same as usual with the addition of the flag `--env=test` so that Symfony knows which variables to use. First create the database, then run all the migrations to match the scema with the live site.

```console
> ./bin/console --env=test doctrine:database:create

> ./bin/console --env=test doctrine:migrations:migrate
```

> Practically there has to be a way to do this programatticaly for CI purposes, that can be figured out later, WP-CLI seems to know how to do it.

## Data Handling in the Test Class

PHPUnit also needs to know what db to connect to but it doesn't have access to the .env.test file, so the same connection needs to be specified in the php section of the `phpunit.xml.dist` file.

```xml
<?xml version="1.0" charset="utf-8" ?>
<phpunit>
    <php>
      <!-- ... -->
      <!-- the value is the Doctrine connection string in DSN format -->
      <env name="DATABASE_URL" value="mysql://db_user:db_pass@127.0.0.1:3306/test_database"/>
    </php>
    <!-- ... -->
</phpunit>
```

### Test Fixtures

For each test the database needs to be in a specific state. Data needs to be added or removed for the test and afterward it needs to be deleted for the next test. PHPUnit provides **Fixtures** to handle these global actions.

| Fixture                | When it runs                        |
| ---------------------- | ----------------------------------- |
| `setUp()`              | Before every test method            |
| `tearDown()`           | Once after every test method        |
| `setUpBeforeClass()`   | Once before any tests have been run |
| `tearDownAfterClass()` | Once after all tests have been run  |

The before and after fixtures are handy for setting up connections (_eg._ to a database) that are used for all tests, then dropping the connection at the end.

### Cleaning Up the Database

In order to test the contents of the database (via the functional testing of the front-end) it is important to be sure of the content in the tables. The simplest way to handle this is to truncate all of the tables prior to each test. Each test will modify the data as necessary to complete its tests.

In preparation for this we'll add two private functions to the test class, `getEntityManager()` and `truncateEntityTables()`. These methods will make use of some external classes so we'll also add the necessary `use` statements.

```php
# New external classes.
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
# End new external classes.
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
  /**
   * @var \Doctrine\ORM\EntityManagerInterface|null $em the entityManager
   */
  private $em;

  /**
   * Do this before all tests
   */
  public function setUp(): void
  {
      // Start the kernel.
      self::bootKernel();

      // Load the entity manager.
      $this->em = $this->getEntityManager();

      // Clear the database tables
      $this->truncateEntityTables();
  }

  # Test methods go here ...

  /**
   * Get the entity Manager
   *
   * @return EntityManager
   */
  private function getEntityManager(): EntityManager
  {
      return self::$kernel->getContainer()
          ->get('doctrine')
          ->getManager();
  }

  /**
   * Truncate all Entity Tables
   */
  private function truncateEntityTables(): void
  {
      $purger = new ORMPurger($this->em);
      $purger->purge();
  }
}
```

### Breaking it Down

In the setup it is necessary for the test class to gain access to the app kernel with the `self::bootKernel()` function.

Next the member variable `$this->em` is instantiated as the doctrine entity manager for this kernel's container. (in `getEntityManager()`). The entity manager is used in multiple tests, so it makes sense to load it up globally for all methods in the class.

Then we clear out all existing data in the test database using the ORMPurger's `purge()` method (in `truncateEntityTables()`)

### Cleaning Up.

In order to avoid memory leaks that may come with continuous re-instantiating of the entity manager it is good practice to clean up after each test by closing it down and resetting the member variable to `null`. This is done in the `tearDown()` method that runs after each test.

```php
/**
 * {@inheritdoc}
 */
protected function tearDown(): void
{
    parent::tearDown();

    // Close down and reset the entity manager to avoid memory leaks.
    $this->em->close();
    $this->em = null;
}
```
