# Building a Drupal Module with Embedded React

## Part 3: Adding the React App

**Goals**

- add the React app code to the module
- install Webpack and Babel to compile the app
- compile the app to get the stand-alone .js file we can link to the module.

## The React Code

The React code for this module has already been created and tested elsewhere using [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html) We are going to re-purpose that code and set up Webpack to compile it as recomended by [redfinsolutions.com](https://redfinsolutions.com/blog/embedding-react-app-drupal-8-site) in their article on embedding react in Drupal.

### Where to put it.

Webpack by default looks for code in the `/src` directory and Drupal modules have one of those. Perhaps we can simply add the react components there.

### Node.js

In order to use React, Webpack, Babel (and various other packages) we need to use node.js and the NPM package manager. If you haven't already you should globally install the latest version of node.js (which comes with NPM) downloading and installing it from [nodejs.org](https://nodejs.org/en/)

Once Node is installed we will initialise the package in the module's root directory, by running `npm init` (which is an interactive program like drush) that will ask us some questions and create a `package.json` file for managing dependancies. If you want it to go faster use the `-y` flag and edit the `package.json` file later.

**IMPORTANT** If you are using version control (and you should be) Node creates a folder called node_modules (like the vendor directory in composer) which contains a LOT of files. You don't want to commit these. Add a `.gitignore` file in the module root which excludes `/node_modules` (You'll thank me later).

### Installing Packages

The React app requires a number of things, namely:

- React _the main React library_
- React-Dom _DOM entry points for React_
- Reactstrap _components that use Bootstrap_
- Bootstrap 4 _popular CSS framework_
- Axios _Promise based library for retreiving data from apis_

While we can install these in one long command I find it better to do it in stages. First React, then Bootstrap, then Reactstrap and finally Axios

```console
npm install react react-dom
npm install bootstrap
npm install reactstrap
npm install axios
```

Bootstrap will complain that it requires a peer of jquery@1.9.1 and popper.js@1.14.7. Reactstrap takes care of the popper.js dependency and Drupal has the jQuery library available so we should be good to go.

As these files are added the `package.json` file will gain a "dependencies" node containing the libraries. Additionally a `package-lock.json` file will be created. These should both be committed to version control.

### Adding Webpack

Webpack is a bundle manager that takes code files and bundles them into a small set of files which make up part (or all) of your app. It is commonly used to transpile JSX and ES6 into vanilla JavaScript files, and for compiling Sass into plain CSS.

We begin by installing Webpack and the Webpack-CLI (another Command Line Intreface like drush) packages as development dependencies.

```console
npm install webpack webpack-cli --save-dev
```

The package.json file now has a `devDependencies` node that lists webpack and webpack-cli.

### webpack.config.js

While webpack can be run from the command line it quickly gets very complicated. To make it easier to use we create a configuration file that exports a json object that contains all of the configuration necesssary. This is a javascript file called `webpack.config.js` and it will live at the root of the module.

Here is the absolute basic webpack config file:

```javascript
module.exports = {
  mode: "development"
};
```

The development mode is set here to stop the webpack script from complaining that the mode is not set.

If we ran webpack right now, it would complain, and it would read `/src/index.js` pull together everything that it includes, bundle it up and deposit the resulting file in `/dist/main.js`.

Before we can run it for the first time we need to add Babel.

### Babel Transpilation.

Babel is a library that takes in various scripting languages (JSX, ES6, Coffescript, TypeScript, _etc._) and converts them to vanilla ES5 javascript that can be understood by any web browser.

To include babel in the project run:

```bash
npm install --save-dev babel-loader @babel/core
```

Once insatlled we can add a "loader" into the "module.rules" section of the webpack config.

```javascript
module.exports = {
  mode: "development",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: "babel-loader"
      }
    ]
  }
};
```

This tells webpack to run any .js files that it finds through the Babel process to convert them into vanilla .js. Note that all of the files inside the `node_modules` folder are ignored.

Babel needs to be set up with its own configuration so that it knows what to do with the incoming files. We only load the rules that we need. This can be done in a separate `.babelrc` file or inside `package.json`. To cut down on files we'll add the following to `package.json` right after the "main" entry.

```json
"babel": {
  "presets": ["@babel/preset-env", "@babel/preset-react"]
},
```

Obviously we also need to add these presets to our project with

```console
npm install @babel/preset-env @babel/preset-react --save-dev
```

### Handling CSS

Since our app uses css we also need to add loaders for webpack to properly bundle that as well. Specifically we need `css-loader` and `style-loader`

Install them in the usual way.

```console
npm install style-loader css-loader --save-dev
```

And add the appropriate rule to the webpack config

```javascript
module: [
  rules: [
    ... ,
    {
      test: /\.css$/,
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' }
      ]
    }
  ]
]
```

### Running Webpack.

The webpack executable lives in the `node_modules/.bin/` directory so running it from the command line looks like:

```console
./node_modules/.bin/webpack
```

To make it simpler we can add the following script definintions to `package.json`

```json
"scripts": {
  "build": "webpack",
  "watch": "webpack -w"
},
```

Now we can run `npm run build` to bundle our app, or `npm run watch` to build and watch the files for changes, (handy in development.)

Now when we run our script we see the following on the command line:

```console
Hash: ad3d02d363b09ac2d5cc
Version: webpack 4.39.2
Time: 1349ms
Built at: 08/16/2019 2:16:08 PM
  Asset      Size  Chunks             Chunk Names
main.js  1.97 MiB    main  [emitted]  main
Entrypoint main = main.js
[./node_modules/css-loader/dist/cjs.js!./src/css/index.css] 1.26 KiB {main} [built]
[./node_modules/webpack/buildin/global.js] (webpack)/buildin/global.js 472 bytes {main} [built]
[./src/constants.js] 488 bytes {main} [built]
[./src/css/index.css] 409 bytes {main} [built]
[./src/functions.js] 2.24 KiB {main} [built]
[./src/index.js] 277 bytes {main} [built]
```

and we see the resulting file `main.js` in the `/dist` folder.

Ordinarily the resuting bundled files are not committed to version control. However, in this case we need that file for the module, so go ahead and commit it.

**Next Up** Wiring up the compiled main.js file into the Drupal module

### [Part 4 »](./DrupalReactModulePart4.md)
