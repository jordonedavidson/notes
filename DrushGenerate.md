# The Drush 9.x Generate Command

Drupal's command line interface `drush` provides the `generate` command to interactively create the boilerplate for various Drupal files.

## Resources

- [Official Documentation](https://drushcommands.com/drush-9x/core/generate/) _very limited_
- [Drupal Code Builder](https://github.com/drupal-code-builder/drupal-code-builder) _The underlying project_

## Drush Generate Options

Drush generate 9.7.1

Run `drush generate [command]` and answer a few questions in order to write starter code to your project.

Available commands:

| \_global:                           |                                |
| ----------------------------------- | ------------------------------ |
| composer (composer.json)            | Generates a composer.json file |
| controller                          | Generates a controller         |
| field                               | Generates a field              |
| hook                                | Generates a hook               |
| install-file                        | Generates an install file      |
| javascript                          | Generates Drupal 8             | JavaScript file |
| layout                              | Generates a layout             |
| migration                           | Generates the yml and PHP      | class for a Migration |
| project                             | Generates a composer project   |
| render-element                      | Generates Drupal 8 render      | element |
| settings-local (settings.local.php) | Generates Drupal 8             | settings.local.php file |
| template                            | Generates a template           |
| theme                               | Generates Drupal 8 theme       |

| drush:                   |                              |
| ------------------------ | ---------------------------- |
| drush-alias-file (daf)   | Generates a Drush site alias | file. |
| drush-command-file (dcf) | Generates a Drush command    | file. |

| form:                       |                                |
| --------------------------- | ------------------------------ |
| form-config (config-form)   | Generates a configuration form |
| form-confirm (confirm-form) | Generates a confirmation form  |
| form-simple                 | Generates simple form          |

| module:                                            |                                       |
| -------------------------------------------------- | ------------------------------------- |
| module-configuration-entity (configuration-entity) | Generates configuration entity module |
| module-content-entity (content-entity)             | Generates content entity module       |
| module-file                                        | Generates a module file               |
| module-standard (module)                           | Generates standard Drupal 8 module    |

| plugin:                                                        |                                             |
| -------------------------------------------------------------- | ------------------------------------------- |
| plugin-action (action)                                         | Generates action plugin                     |
| plugin-block (block)                                           | Generates block plugin                      |
| plugin-ckeditor (ckeditor)                                     | Generates CKEditor plugin                   |
| plugin-condition (condition)                                   | Generates condition plugin                  |
| plugin-constraint (constraint)                                 | Generates constraint plugin                 |
| plugin-entity-reference-selection (entity-reference-selection) | Generates entity reference selection plugin |
| plugin-field-formatter (field-formatter)                       | Generates field formatter plugin            |
| plugin-field-type (field-type)                                 | Generates field type plugin                 |
| plugin-field-widget (field-widget)                             | Generates field widget plugin               |
| plugin-filter (filter)                                         | Generates filter plugin                     |
| plugin-manager                                                 | Generates plugin manager                    |
| plugin-menu-link (menu-link)                                   | Generates menu-link plugin                  |
| plugin-migrate-destination (migrate-destination)               | Generates migrate destination plugin        |
| plugin-migrate-process (migrate-process)                       | Generates migrate process plugin            |
| plugin-migrate-source (migrate-source)                         | Generates migrate source plugin             |
| plugin-queue-worker (queue-worker)                             | Generates queue worker plugin               |
| plugin-rest-resource (rest-resource)                           | Generates rest resource plugin              |
| plugin-views-argument-default (views-argument-default)         | Generates views default argument plugin     |
| plugin-views-field (views-field)                               | Generates views field plugin                |
| plugin-views-style (views-style)                               | Generates views style plugin                |

| service:                                          |                                         |
| ------------------------------------------------- | --------------------------------------- |
| service-access-checker (access-checker)           | Generates an access checker service     |
| service-breadcrumb-builder (breadcrumb-builder)   | Generates a breadcrumb builder service  |
| service-cache-context (cache-context)             | Generates a cache context service       |
| service-custom (custom-service)                   | Generates a custom Drupal service       |
| service-event-subscriber (event-subscriber)       | Generates an event subscriber           |
| service-logger (logger)                           | Generates a logger service              |
| service-middleware (middleware)                   | Generates a middleware                  |
| service-param-converter (param-converter)         | Generates a param converter service     |
| service-path-processor (path-processor)           | Generates a path processor service      |
| service-provider                                  | Generates a service provider            |
| service-request-policy (request-policy)           | Generates a request policy service      |
| service-response-policy (response-policy)         | Generates a response policy service     |
| service-route-subscriber (route-subscriber)       | Generates a route subscriber            |
| service-theme-negotiator (theme-negotiator)       | Generates a theme negotiator            |
| service-twig-extension (twig-extension)           | Generates Twig extension service        |
| service-uninstall-validator (uninstall-validator) | Generates a uninstall validator service |

| test:                             |                                           |
| --------------------------------- | ----------------------------------------- |
| test-browser (browser-test)       | Generates a browser based test            |
| test-kernel (kernel-test)         | Generates a kernel based test             |
| test-nightwatch (nightwatch-test) | Generates a nightwatch test               |
| test-unit (unit-test)             | Generates a unit test                     |
| test-web (web-test)               | Generates a web based test                |
| test-webdriver (webdriver-test)   | Generates a test that supports JavaScript |

| theme:         |                                            |
| -------------- | ------------------------------------------ |
| theme-file     | Generates a theme file                     |
| theme-settings | Generates Drupal 8 theme-settings.php file |

| yml:                                    |                                     |
| --------------------------------------- | ----------------------------------- |
| yml-breakpoints (breakpoints)           | Generates a breakpoints yml file    |
| yml-links-action (action-links)         | Generates a links.action yml file   |
| yml-links-contextual (contextual-links) | Generates links.contextual yml file |
| yml-links-menu (menu-links)             | Generates a links.menu yml file     |
| yml-links-task (task-links)             | Generates a links.task yml file     |
| yml-module-info (module-info)           | Generates a module info yml file    |
| yml-module-libraries (module-libraries) | Generates module libraries yml file |
| yml-permissions (permissions)           | Generates a permissions yml file    |
| yml-routing (routing)                   | Generates a routing yml file        |
| yml-services (services)                 | Generates a services yml file       |
| yml-theme-info (theme-info)             | Generates a theme info yml file     |
| yml-theme-libraries (theme-libraries)   | Generates theme libraries yml file  |
