# Hostsmart Site Migration Proceedure

## On Hostsmart

1. Log into the admin at [meanandugly.com](https://www.meanandugly.com:8443)
2. Click `Tools and Setting` on the left menu bar under `Server Management`
3. Under the `Tools & Resources` Click on `Migration & Transfer Manager`

### In the Migrator

1. Click on the IP address (64.34.221.25) under the **Host** column.
2. Locate the domain that you want to migrate, and click `[Re-Sync]` on that line
3. Click `OK` on the modal popup that appears. (The existing checkboxes are fine)
4. The migration process happens under the `Migrations queue`
5. Once complete, there eill be a green checkmark beside the domain.

### In the Subscription 

This is where we set up the Domain Administrator account

1. Click on the `Subscriptions` Link in the sidebar Menu
2. Click on the domain in the list
3. Click on the Users Tab across the top.
4. Set the `Entries Per Page` to **All**
5. Use the browser's find feature `CTRL-F` to search for the domain name.
6. Locate the Account with the Role (3rd column) `Domain Administrator (domain name)`
7. Click on the username.
8. Click on the `Change Settings` Button
9. Set the **User Role** to `Admin - Web` from the pop-up
10 At the bottom of the page Click `Apply` then `OK`

### Update the DNS

1. Click on the Domain in the breadcrumb at the top of the page
2. Click on the `DNS` item in the set of Icons (You may need to click `Show More`)
3. _Ignore the Warning_
4. The 2 **NS** Records need to be updated. By clicking on the domain name in the 1st column change:
    - `ns1.hostsmart.ca` to `ns1.meanandugly.com`
    - `ns2.hostsmart.ca` to `ns5.hostsmart.ca`
5. In the Top warning that appeared click the `Update` button to fully apply the changes.


## On Enom

1. Login to [enom.com](https://www.enom.com)
2. In the Account overview, enter the domain in th `Search My Domains` box
3. Go into the `DNS Server Settings`
4. Update the name servers to:
    - `ns1.meanandugly.com`
    - `ns5.hostsmart.ca`
5. Click `Save`
6. Click `Yes` on the alert to continue.

Once it shows `Updated Successfully` you are done here.

## In the Future

It will take up to 48 hours for the domain to fully transfer to point to the new server.

Once you are certain that the domain is resolving to the new server login to the admin on [Old HostSmart](https://www.hostsmart.ca:8443). Navigate to the Domain list and disable the domain.

**_Important_** If the client has email with HostSmart they need to be given the Email update instructions to prepare for the transfer.