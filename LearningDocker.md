# Learning Docker

> From the linked in course **Docker for Developers**

## Dockerfiles

The `Dockerfile` is a recipe for creating a docker container image.
It runs through the process, step-by-step, to setup the developement (or production)
environment you will be using.

### Sample

```docker
FROM node
# The base docker image to start from. Always the first instruction
WORKDIR /usr/src/app
# The working directory in the container
COPY package*.json ./
# Copy local files into the container
RUN npm install
# Run a command in the container (in the working directory)
COPY . .
EXPOSE 4000
# Expose the port inside the container that can be mapped to a port on the host machine
CMD [ "npm", "start"]
# Run a command form the command line in the container.
```

### Buiding an image

Run the `docker build` command to create a container image from the `Dockerfile` recipe. This command will build the image `manny/simple-backend` from the dockerfile in the current directory. Like Composer packages, Docker images are in the format `<vendor>/<container name>`.

```console
docker build -t manny/simple-backend .
```

## Commands Respecting Images.

- `docker images` list all available images on your system
- `docker rmi <image id>` Remove the specified image

## Starting up a Container

From the command line `docker run <image name>` starts up the specified container. There are a number of flags that can be passed ro the command to setup various configurations for the container. The most common is the `-p` flag which is used to map the exposed ports from the container to the host system. _ex._:

```console
docker run -p 4000:4000 manny/simple-backend
```

The first port is for the real world and the second is the port exposed inside the container

## Common Commands

- `docker run <image name>` Builds and starts the image
- `docker ps` List all runnning containers
- `docker stop <container id>` Stop the specified container
- `docker start <container id>` Starts the specified container
- `docker push <image name>` Push a docker image to docker hub
- `docker pull <image name>` Pull a docker image from docker hub

The container id for the stop and start commands only needs to be enough characters to uniquely identify it from the list.

Other repositorys and registries than docker hub exist. images may be pushed and pulled to them by specifiying them in the command.

> The full list of [docker commands](https://docs.docker.com/engine/reference/commandline/docker/)

## docker-compose.yml

In order to set up a consistent system of containers we use a YAML file that contains the options necessary to start the images. Essentially this takes the place of running the commands by hand from the command line.

This is optimal when there are several inter connected containers that need to be deployed. _ex._ A backend application and a datasource.

```yaml
app:
  container_name: app
  restart: always
  build: .
  ports:
    - "4000:4000"
  links:
    - mongo

mongo:
  container_name: mongo
  image: mongo
  expose:
    - "27017"
  volumes:
    - ./data:/data/db
  ports:
    - "27017:27017"
```

The links section in the `app` connects it to other containers. The exposed ports in those containers are mapping into the app container. In the example above the app connects to mongo via its internal port 27017. That port is not available to the outside world machine running the app container.

To build all the services from the dockerfile run `docker-compose build` in the same directory.

To run the containers run `docker-compose up`. The `-d` flag runs the containers in "Detatched" mode _i.e_ in the background. Passing an image name to the command will start the specified image.

Containers may be stopped with `docker-compose down`.

> `up` pulls the images and starts them. `down` stops the images and removes them.

### docker-compose Command Parameters

| Command | Effect                                                    |
| ------- | --------------------------------------------------------- |
| build   | Build or rebuild services                                 |
| bundle  | Generate a Docker bundle from the Compose file            |
| config  | Validate and view the Compose file                        |
| create  | Create services                                           |
| down    | Stop and remove containers, networks, images, and volumes |
| events  | Receive real time events from containers                  |
| exec    | Execute a command in a running container                  |
| help    | Get help on a command                                     |
| images  | List images                                               |
| kill    | Kill containers                                           |
| logs    | View output from containers                               |
| pause   | Pause services                                            |
| port    | Print the public port for a port binding                  |
| ps      | List containers                                           |
| pull    | Pull service images                                       |
| push    | Push service images                                       |
| restart | Restart services                                          |
| rm      | Remove stopped containers                                 |
| run     | Run a one-off command                                     |
| scale   | Set number of containers for a service                    |
| start   | Start services                                            |
| stop    | Stop services                                             |
| top     | Display the running processes                             |
| unpause | Unpause services                                          |
| up      | Create and start containers                               |
| version | Show the Docker-Compose version information               |
